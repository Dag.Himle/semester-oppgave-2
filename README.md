# Ant Simulation Project

This project is a simulation of ants, their colonies, and their environment. The ants can search for food, bring it back to their colonies, and interact with other ants and obstacles in the environment. The simulation includes a graphical user interface (GUI) for creating custom maps and visualizing the simulation in real-time.
- The algorithm for the ants' behavior is loosely based on the following paper:
    - https://arxiv.org/pdf/1303.4969.pdf
- And inspired by the following video:
    - https://youtu.be/81GQNPJip2Y


## Features

- Create custom maps with colonies, food caches, and walls
- Real-time visualization of ant behavior and interactions
- Save and load custom maps
- Map preview with details such as name, dimensions, and object locations

### Link to Demo Video
- https://youtu.be/uk8su6bd3lQ


## Getting Started

## Usage

1. Run the simulation and the GUI will appear.
2. Select "Create a new map" to start designing a custom map. (Alternatively, you can select "Load a map" to load a previously saved map.)
3. Use the interface to add colonies, food caches, and walls to your map.
4. Save your custom map to a file, or load a previously saved map.
5. Start the simulation and watch the ants interact with their environment.
