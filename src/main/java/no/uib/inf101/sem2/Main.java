package no.uib.inf101.sem2;


import javax.swing.SwingUtilities;
import no.uib.inf101.sem2.view.MainFrame;

public class Main {
  public static void main(String[] args) {

    SwingUtilities.invokeLater(() -> {
        MainFrame frame = new MainFrame();
        frame.setVisible(true);
    });
}

}
