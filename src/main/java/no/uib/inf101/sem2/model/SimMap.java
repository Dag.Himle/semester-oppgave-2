package no.uib.inf101.sem2.model;

import java.util.List;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;

import no.uib.inf101.sem2.entities.Colony;
import no.uib.inf101.sem2.entities.Food;
import no.uib.inf101.sem2.entities.IDrawableChar;
import no.uib.inf101.sem2.entities.Wall;
import no.uib.inf101.sem2.utils.records.*;



public class SimMap {
    //Expose is used to make the fields visible to Gson
    @Expose
    private String name;
    @Expose
    private List<Colony> colonies;
    @Expose
    private List<Food> foodCaches;
    @Expose
    private List<Wall> walls;
    @Expose
    private MapDimension size;


    /**
     * Constructor for a map with a name, size, colonies, food caches and walls.
     * @param colonies
     * @param foodCaches
     * @param walls
     * @param size
     * @param name
     */
    public SimMap(List<Colony> colonies, List<Food> foodCaches, List<Wall> walls, MapDimension size, String name) {
        this.size = size;
        this.name = name;
        this.colonies = colonies;
        this.foodCaches = foodCaches;
        this.walls = walls;
    }

    public SimMap(int width, int height) {
        this.size = new MapDimension(width, height);
        this.name = null;
        this.colonies = new ArrayList<>();
        this.foodCaches = new ArrayList<>();
        this.walls = new ArrayList<>();
    }

    /**
     * Constructor for a an empty map named "Untitled" with a size of 400x400.
     */
    public SimMap() {
        this.name = "Untitled";
        this.size = new MapDimension(400, 400);
        this.colonies = new ArrayList<>();
        this.foodCaches = new ArrayList<>();
        this.walls = new ArrayList<>();
    }

    /**
     * Getter for the name of the map.
     * @return The name of the map as a string.
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name of the map.
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the colonies on the map.
     * @return A list of colonies.
     */
    public List<Colony> getColonies() {
        return colonies;
    }

    /**
     * Getter for the food caches on the map.
     * @return A list of food caches.
     */
    public List<Food> getFood() {
        return foodCaches;
    }

    /**
     * Getter for the walls on the map.
     * @return A list of walls.
     */
    public List<Wall> getWalls() {
        return walls;
    }

    /**
     * Getter for the size of the map.
     * @return A MapDimension object containing the size of the map.
     */
    public MapDimension getSize() {
        return size;
    }

    /**
     * Getter for the width of the map.
     * @return The width of the map as an integer.
     */
    public int getWidth() {
        return size.width();
    }

    /**
     * Getter for the height of the map.
     * @return The height of the map as an integer.
     */
    public int getHeight() {
        return size.height();
    }

    /**
     * Getter for the center of the map.
     * @return A Position object containing the center of the map.
     */
    public Position getCenter() {
        return new Position(size.width() / 2, size.height() / 2);
    }

    /**
     * Adds a colony to the map, assumes the given position is to be the center of the colony.
     * @param position
     * @param size
     * @param color
     */
    public void addColony(Position center, int size, String color) {
        Colony newcolony = new Colony(center, size, color);
        colonies.add(newcolony);
    }

    /**
     * Adds a food cache to the map, assumes the given position is to be the center of the food cache.
     * @param position
     * @param amount
     */
    public void addFood(Position position, int amount) {
        Food newFood = new Food(position, amount);
        foodCaches.add(newFood);
    }

    /**
     * Adds a wall to the map. Assumes the given position is to be the center of the wall.
     * @param position
     * @param size
     */
    public void addWall(Position position, int size) {
        Wall newWall = new Wall(position, size);
        walls.add(newWall);
    }

    /**
     * Removes a colony from the map
     * @param colony
     */
    public void removeColony(Colony colony) {
        colonies.remove(colony);
    }

    /**
     * Removes a food cache from the map
     * @param food
     */
    public void removeFood(Food food) {
        foodCaches.remove(food);
    }

    /**
     * Removes a wall from the map
     * @param wall
     */
    public void removeWall(Wall wall) {
        walls.remove(wall);
    }

    /**
     * Sets the size of the map
     * @param size
     */
    public void setSize(MapDimension size) {
        this.size = size;
    }

    /**
     * Sets the colonies on the map
     * @param colonies
     */
    public void setColonies(List<Colony> colonies) {
        this.colonies = colonies;
    }

    /**
     * Sets the food caches on the map
     * @param foodCaches
     */
    public void setfoodCaches(List<Food> foodCaches) {
        this.foodCaches = foodCaches;
    }

    /**
     * Sets the walls on the map
     * @param walls
     */
    public void setWalls(List<Wall> walls) {
        this.walls = walls;
    }

    /**
     * Checks if a position is within the bounds of the map.
     * @param pos
     * @return True if the position is within the bounds of the map, false otherwise.
     */
    public boolean isPositionInBounds(Position pos) {
        return pos.x() > 1 && pos.x() < size.width() && pos.y() > 1 && pos.y() < size.height();
    }

    /**
     * Gets the drawable objects on the map. Used for drawing map previews.
     * @return An iterable of drawable objects.
     */
    public Iterable<IDrawableChar> getDrawables(){
        List<IDrawableChar> drawables = new ArrayList<>();
        drawables.addAll(colonies);
        drawables.addAll(foodCaches);
        drawables.addAll(walls);
        return drawables;
    }

    /**
     * Deletes the object at the given position.
     * @param pos
     */
    public void deleteObjectAt(Position pos) {
        // Remove colony if it contains the position
        for (Colony colony : colonies) {
            if (colony.contains(pos)) {
                colonies.remove(colony);
                break;
            }
        }

        // Remove food cache if it contains the position
        for (Food food : foodCaches) {
            if (food.contains(pos)) {
                foodCaches.remove(food);
                break;
            }
        }

        // Remove wall if its position is equal to the given position
        for (Wall wall : walls) {
            if (wall.contains(pos)) {
                walls.remove(wall);
                break;
            }
        }
    }
}
