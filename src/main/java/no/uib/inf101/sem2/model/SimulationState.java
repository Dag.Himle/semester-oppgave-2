package no.uib.inf101.sem2.model;

public enum SimulationState {
    MENU,
    RUNNING,
    SCENARIO,
    PAUSED,
    INFO,
    SETTNGS
}
