package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.utils.records.*;
import no.uib.inf101.sem2.view.AntSimulationPanel;
import no.uib.inf101.sem2.entities.Ant;
import no.uib.inf101.sem2.entities.Colony;
import no.uib.inf101.sem2.entities.Food;
import no.uib.inf101.sem2.entities.IDrawableChar;
import no.uib.inf101.sem2.entities.Wall;
import no.uib.inf101.sem2.entities.Pheromone;
import no.uib.inf101.sem2.utils.AntSense;
import no.uib.inf101.sem2.utils.SimMapIO;

import javax.swing.Timer;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;


/**
 * The model for the ant simulation.
 * This class is responsible for the ants and pheromones.
 * It also keeps track of the colonies and the food sources.
 */
public class AntSimulation {

    // Fields
    public SimulationState state;
    private SimMap simMap;
    private List<Colony> colonies;
    private PheromoneGrid pheromoneGrid;
    private Timer timer;
    private Random random;
    private AntSimulationPanel simulationPanel;
    private List<Food> foodCaches;
    private List<Wall> walls;
    private HashMap<String, Integer> antCount = new HashMap<>();

    // Multi-threading
    private ExecutorService executorService;

    
    // Simulation constants
    private static final int UPDATE_INTERVAL = 36; // The interval between updates in milliseconds
    private static final int PHEROMONE_GRID_CELL = 10; // The size of each cell in the pheromone grid

    // Ant constants
    private static final int ANT_SIGHT_RANGE = 20; // The range at which ants can see pheromones
    private static final int ANT_SPEED = 2; // The speed of ants
    private static final double ANT_EDGE_AVERSION_RANGE = 10; // The range at which ants will avoid the edge of the map

    // Behaviour coefficients
    private static final double ANT_RANDOM_COEFFICIENT = 0.6; // The coefficient used to determine how much randomness ants have when choosing a direction
    private static final double ANT_PHEROMONE_COEFFICIENT = 0.3; // The coefficient used to determine how much pheromones affect the direction of ants
    private static final double ANT_MOMENTUM_COEFFICIENT = 0.1; // The coefficient used to determine how much momentum ants have when choosing a direction


    // Constructors
    /**
     * Creates a new simulation with the provided map.
     * <p> This constructor is used when a map is provided.
     * @param simMap
     */
    public AntSimulation(SimMap simMap) {
        //Initialize the thread pool
        this.executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        this.simMap = simMap;
        this.colonies = new ArrayList<>();
        this.pheromoneGrid = new PheromoneGrid(PHEROMONE_GRID_CELL, simMap.getWidth()/PHEROMONE_GRID_CELL, simMap.getHeight()/PHEROMONE_GRID_CELL);
        this.foodCaches = new ArrayList<>();
        this.walls = new ArrayList<>();
        this.random = new Random();
        initializeSimulation();
    }

    /**
     * Creates a new simulation with the first map in the maps folder.
     * If no maps are found, a new map is created.
     * <p> This constructor is used when no map is provided.
     */
    public AntSimulation() {
        //Initialize the thread pool
        this.executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        // If Map is not provided load the first map in the maps folder
        if (this.simMap == null) {
            if (SimMapIO.getAvailableMaps().length > 0) {
                System.out.println("Loading map: " + SimMapIO.getAvailableMaps());
                this.simMap = SimMapIO.loadMap(SimMapIO.getAvailableMaps()[0]);
            } else {
                this.simMap = new SimMap(800, 800);
            }
        }
        this.colonies = new ArrayList<>();
        this.pheromoneGrid = new PheromoneGrid(PHEROMONE_GRID_CELL, simMap.getWidth()/PHEROMONE_GRID_CELL, simMap.getHeight()/PHEROMONE_GRID_CELL);
        this.foodCaches = new ArrayList<>();
        this.walls = new ArrayList<>();
        this.random = new Random();
        initializeSimulation();
    }


    /**
     * Initializes the simulation. And starts the timer for the simulation.
     */
    public void start() {
        ActionListener timerListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (state == SimulationState.RUNNING) {
                    step(true);
                }
            }
        };
        timer = new Timer(UPDATE_INTERVAL, timerListener);
        timer.start();
    }

    /**
     * Perform a single step in the simulation.
     * This method is called by the timer.
     * <p> This method is responsible for updating the pheromones, ants and food caches.
     * It also spawns new ants.
     * <p> This method is also responsible for repainting the simulation.
     * For testing purposes, the repaint parameter can be set to false.
     * @param repaint
     */
    public void step(boolean repaint){
        pheromoneGrid.updatePheromones();
        updateAnts();
        spawnNewAnts();
        if (repaint) {
            simulationPanel.repaint();
        }
    }

    /**
     * Sets the simulation to paused
     * @param paused
     */
    public void setPaused(boolean paused) {
        if (paused) {
            state = SimulationState.PAUSED;
        } else {
            state = SimulationState.RUNNING;
        }
    }

    /**
     * Returns a boolean for whether the simulation is paused or not.
     * @return
     */
    public boolean isPaused() {
        if (state == SimulationState.PAUSED) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Gives the simulation access to the 
     * @param simulationPanel
     */
    public void setView(AntSimulationPanel simulationPanel) {
        this.simulationPanel = simulationPanel;
    }

    /**
     * Returns all entities in the simulation that can be drawn.
     * This includes colonies, food sources, walls, ants, and pheromones.
     * <p> Uses the {@link IDrawableChar} interface to only return relevant data to the simulationPa.
     * @return A list of all drawable entities in the simulation.
     */
    public List<IDrawableChar> getDrawableEntities() {
        List<IDrawableChar> drawableEntities = new ArrayList<>();
        drawableEntities.addAll(colonies);
        drawableEntities.addAll(foodCaches);
        drawableEntities.addAll(walls);
        drawableEntities.addAll(pheromoneGrid.getDrawablePheromones());
        return drawableEntities;
    }

    private void initializeSimulation() {
        // Clear any existing colonies, food, antcounts and walls
        colonies.clear();
        foodCaches.clear();
        walls.clear();
        antCount.clear();

        // Initialize colonies from the SimMap and update antCount hasmap for statistics
        for (Colony colony : simMap.getColonies()) {
            colonies.add(colony);
            this.antCount.put(colony.getColorString(), 0);
        }
    
        // Initialize food sources from the SimMap
        for (Food food : simMap.getFood()) {
            foodCaches.add(food);
        }
    
        // Initialize walls from the SimMap
        for (Wall wall : simMap.getWalls()) {
            walls.add(wall);
        }

        if (simulationPanel != null) {
            simulationPanel.updateStatsPanel();
        }
    }


    /**
     * Updates all ants in each colony, adding any new pheromones created during
     * the update process to the pheromone grid.
     * <p>
     * This method first clears the ant count, and creates an empty list to store
     * new {@link Pheromone} objects created during the update process. It then
     * loops through all {@link Colony} objects and their respective {@link Ant}s,
     * submitting the {@link #updateAnt(Ant)} method to be executed by the
     * {@link java.util.concurrent.ExecutorService}. The result, an
     * {@link java.util.Optional}<{@link Pheromone}>, is added to a list of
     * {@link java.util.concurrent.Future} objects.
     * <p>
     * After all ants have been updated, the method waits for the completion of
     * all {@link java.util.concurrent.Future} objects, and if a new
     * pheromone object was created during the update process, it is added
     * to the list of new pheromones. Finally, the new pheromones are added to the
     * {@link PheromoneGrid}
     */
    private void updateAnts() {
        antCount.clear();
        List<Pheromone> newPheromones = new ArrayList<>();
    
        List<Future<Optional<Pheromone>>> futures = new ArrayList<>();
        for (Colony colony : colonies) {
            for (Ant ant : colony.getAnts()) {
                Future<Optional<Pheromone>> future = executorService.submit(() -> {
                    return updateAnt(ant);
                });
                futures.add(future);
            }
        }
        // Wait for all ant updates to complete
        for (Future<Optional<Pheromone>> future : futures) {
            try {
                Optional<Pheromone> pheromone = future.get();
                pheromone.ifPresent(newPheromones::add);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        pheromoneGrid.addPheromones(newPheromones);
    }

    public void shutdown() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(30, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
        }
    }
    
    


    /**
     * Returns a hashmap containing the number of ants for each colony type.
     * The key is the color string of the colony.
     * The value is the number of ants in the colony.
     * <p> This method is used by the simulationPa to display the number of ants for each colony.
     * @return
     */
    public HashMap<String, Integer> getAntCount() {
        return antCount;
    }

    /**
     * Returns the current map of the simulation.
     * @return
     */
    public SimMap getMap() {
        return simMap;
    }

    /**
     * Loades a map from a file.
     * If the map is successfully loaded, the simulation will be reset.
     * The map must be have the .json extension.
     * <p> The map will be loaded from the json/maps folder.
     * @param mapName
     */
    public void loadMap(String mapName) {
        SimMap newMap = SimMapIO.loadMap(mapName);
        if (newMap != null) {
            simMap = newMap;
            pheromoneGrid = new PheromoneGrid(PHEROMONE_GRID_CELL, simMap.getWidth()/PHEROMONE_GRID_CELL, simMap.getHeight()/PHEROMONE_GRID_CELL);
        } else {
            System.out.println("Failed to load map: " + mapName);
        }
        initializeSimulation();
    }

    /**
     * Returns a list of all colonies in the simulation.
     * @return A list of all the {@link Colony} objects in the simulation.
     */
    public List<Colony> getColonies() {
        return colonies;
    }
    
    private void spawnNewAnts() {
        for (Colony colony : colonies) {
            colony.updateAntCount();
            antCount.merge(colony.getColorString(), colony.getAnts().size(), Integer::sum);
            }
    }
    private boolean wallCollision(Position pos) {
        for (Wall wall : walls) {
            if (wall.contains(pos)) {
                return true;
            }
        }
        return false;
    }

    private Food foodCollision(Ant ant) {
        for (Food food : foodCaches) {
            if (ant.getRadius() + food.getRadius() > ant.getCenter().distance(food.getCenter())) {
                return food;
            }
        }
        return null;
    }

    private Colony colonyCollision(Ant ant) {
        for (Colony colony : colonies) {
            if (colony.contains(ant.getCenter()) && colony.getColorString() == ant.getColonyColor()) {
                return colony;
            }
        }
        return null;
    }

    
    /**
     * Given a position returns the distance to the nearest edge of the map.
     * @param pos
     * @return
     */
    private double mapEdgeRange(Position pos) {
        int mapWidth = simMap.getWidth();
        int mapHeight = simMap.getHeight();
        double x = pos.x();
        double y = pos.y();
        double xRange = Math.min(x, mapWidth - x);
        double yRange = Math.min(y, mapHeight - y);
        return Math.min(xRange, yRange);
    }


    private Optional<Pheromone> updateAnt(Ant ant) {

        // Get the ant's current position and direction
        Position antPosition = ant.getCenter();
        double antDirection = ant.getDirection();

        // Check for collisions with food colonies and walls
        Food food = foodCollision(ant);
        if (food != null){
            if (!ant.isCarryingFood()){
                ant.pickUpFood();
                food.removeFood(1);
            }
        }else {
            Colony colony = colonyCollision(ant);
            if (colony != null && ant.isCarryingFood()){
                colony.addSize(1);
                ant.dropFood();
            }
        }
        // Generate a random direction for the ant to move in
        double randomDirection = 0;
        // If the ant is close to the edge of the map, move towards the center
        if (mapEdgeRange(antPosition) < ANT_EDGE_AVERSION_RANGE){
            randomDirection = antPosition.directionTo(new Position(simMap.getWidth()/2, simMap.getHeight()/2));
        } else {
            randomDirection = ant.getDirection() + (random.nextDouble() * 2 - 1) * (Math.PI * 2);
        }

        // Get the pheromone direction
        double pheromoneDirection = AntSense.getPheromoneDirectionLowestStrength(ant, pheromoneGrid, ANT_SIGHT_RANGE, colonies, foodCaches);

        // Calculate the new direction
        double newDirection = antDirection;
        newDirection = antDirection * ANT_MOMENTUM_COEFFICIENT +
                 pheromoneDirection * ANT_PHEROMONE_COEFFICIENT +
                 randomDirection * ANT_RANDOM_COEFFICIENT;
        
        // Update the ant's direction
        ant.setDirection(newDirection);

        // Update the ant's position
        Position antMove = ant.caluclateMove(ANT_SPEED);
        if (simMap.isPositionInBounds(antMove)) {
            if (wallCollision(antMove)) {
                ant.setDirection(ant.getDirection() + 90);
                antMove = ant.caluclateMove(ANT_SPEED);
            } else {
                ant.updatePosition(antMove);
            }
        }

        if (ant.shouldDropPheromone()) {
            Pheromone pheromone = ant.isCarryingFood()
                ? new Pheromone(Pheromone.Type.TO_FOOD, ant.getCenter(), ant.getColonyColor())
                : new Pheromone(Pheromone.Type.TO_HOME, ant.getCenter(), ant.getColonyColor());

            return Optional.of(pheromone);
        }
        return Optional.empty();
    }
}
