package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.entities.IDrawableChar;
import no.uib.inf101.sem2.entities.Pheromone;
import no.uib.inf101.sem2.utils.records.Position;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The PheromoneGrid class represents a grid of pheromones to optimize ant simulation.
 * The grid is divided into cells of a given size. Each cell contains a list of pheromones.
 * <p> The class is used to optimize the pheromone sensing by minimizing the N^2 complexity
 */
public class PheromoneGrid {
    private final int cellSize;
    private final int gridWidth;
    private final int gridHeight;
    private List<Pheromone>[][] grid;

    /**
     * Constructs a new PheromoneGrid with the specified dimensions and cell size.
     * @param width The width of the grid.
     * @param height The height of the grid.
     * @param cellSize The size of each cell in the grid.
     */
    public PheromoneGrid(int cellSize, int gridWidth, int gridHeight) {
        this.cellSize = cellSize;
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;

        //@SuppressWarnings("unchecked")
        grid = (ArrayList<Pheromone>[][]) new ArrayList<?>[gridWidth][gridHeight];

        for (int x = 0; x < gridWidth; x++) {
            for (int y = 0; y < gridHeight; y++) {
                grid[x][y] = new ArrayList<>();
            }
        }
    }
    /**
     * Adds a list of pheromones to the grid.
     * @param newPheromones The list of pheromones to add.
     */
    public void addPheromones(List<Pheromone> newPheromones) {
        for (Pheromone pheromone : newPheromones) {
            addPheromone(pheromone);
        }
    }


    /**
     * Adds a pheromone to the grid.
     * @param pheromone The pheromone to add.
     */
    public void addPheromone(Pheromone pheromone) {
        Position pos = pheromone.getPosition();
        int cellX = (int) pos.x() / cellSize;
        int cellY = (int) pos.y() / cellSize;
        grid[cellX][cellY].add(pheromone);
    }

    /**
     * Returns a list of pheromones within a certain radius from the specified position.
     * @param position The position to search around.
     * @param radius The search radius.
     * @param type The type of pheromone to search for.
     * @return A list of pheromones within the specified radius.
     */
    public List<Pheromone> getPheromonesInRadius(Position position, double radius) {
        int centerX = (int) position.x() / cellSize;
        int centerY = (int) position.y() / cellSize;
        int cellsRadius = (int) Math.ceil(radius / cellSize);

        List<Pheromone> pheromonesInRadius = new ArrayList<>();

        for (int x = Math.max(0, centerX - cellsRadius); x < Math.min(gridWidth, centerX + cellsRadius + 1); x++) {
            for (int y = Math.max(0, centerY - cellsRadius); y < Math.min(gridHeight, centerY + cellsRadius + 1); y++) {
                for (Pheromone pheromone : grid[x][y]) {
                    if (position.distance(pheromone.getPosition()) <= radius) {
                        pheromonesInRadius.add(pheromone);
                    }
                }
            }
        }

        return pheromonesInRadius;
    }

    /**
     * Decays and removes pheromones from the grid.
     */
    public void updatePheromones() {
        for (int x = 0; x < gridWidth; x++) {
            for (int y = 0; y < gridHeight; y++) {
                Iterator<Pheromone> iterator = grid[x][y].iterator();
                while (iterator.hasNext()) {
                    Pheromone pheromone = iterator.next();
                    if (pheromone.decay()) {
                        iterator.remove();
                    }
                }
            }
        }
    }

    /**
     * Returns a list of drawable pheromones.
     * <p> This method is used to draw the pheromones on the screen.
     * Using the {@link IDrawableChar} interface, the pheromones can be drawn as characters.
     * @return A list of drawable pheromones.
     */
    public List<IDrawableChar> getDrawablePheromones() {
        List<IDrawableChar> drawablePheromones = new ArrayList<>();
        for (int x = 0; x < gridWidth; x++) {
            for (int y = 0; y < gridHeight; y++) {
                for (Pheromone pheromone : grid[x][y]) {
                    drawablePheromones.add((IDrawableChar) pheromone);
                }
            }
        }
        return drawablePheromones;
    }
}