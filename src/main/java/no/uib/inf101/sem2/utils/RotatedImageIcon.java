package no.uib.inf101.sem2.utils;


import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

/**
 * A class that extends the java swing {@link ImageIcon} class and allows for rotation and scaling of the image.
 * The image is rotated around its center. The rotation and scalation utilises the {@link AffineTransform} class.
 * With {@link RenderingHints} to preserve the quality of the image.
 * @author Dag O.B.H. 
 */
public class RotatedImageIcon extends ImageIcon {

    private double initialAngle;
    private Image originalImage;

    public RotatedImageIcon(Image image, double initialAngle) {
        super(image);
        this.originalImage = image;
        this.initialAngle = initialAngle;
    }

    public RotatedImageIcon(String filename, double initialAngle) {
        super(filename);
        this.originalImage = getImage();
        this.initialAngle = initialAngle;

    }

    public RotatedImageIcon(Image image) {
        this(image, 180);
    }

    public RotatedImageIcon(String filename) {
        this(filename, 180);
    }

    public double getInitialAngle() {
        return initialAngle;
    }


    /**
     * Rotates the image by the specified angle.
     * @param angle The angle to rotate the image by
     * @return The rotated image
     */
    public RotatedImageIcon rotate(double angle) {
        Image image = originalImage;
        BufferedImage rotatedImage = rotateImage(image, angle);
        double newInitialAngle = (initialAngle + angle) % 360;
        return new RotatedImageIcon(rotatedImage, newInitialAngle);
    }

    /**
     * Rotates the image to the specified angle.
     * @param targetAngle The angle to rotate the image to
     * @return The rotated image
     * @see #rotate(double)
     */
    public RotatedImageIcon rotateTo(double targetAngle) {
        double rotationAngle = targetAngle - initialAngle;
        return rotate(rotationAngle);
    }

    /**
     * Scales the image by the specified value.
     * The value is a percentage of the original size represented as an integer.
     * @param scaleValue The value to scale the image by
     * @return The scaled image as an {@link RotatedImageIcon}
     */
    public RotatedImageIcon scale(int scaleValue) {
        double scaleFactor = scaleValue / 100.0;
        int newWidth = (int) (getImage().getWidth(null) * scaleFactor);
        int newHeight = (int) (getImage().getHeight(null) * scaleFactor);

        Image scaledImage = getImage().getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
        return new RotatedImageIcon(scaledImage, initialAngle);
    }
    

    private static BufferedImage rotateImage(Image image, double angle) {
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        double radians = Math.toRadians(angle);
    
        int newWidth = (int) (Math.abs(Math.cos(radians) * width) + Math.abs(Math.sin(radians) * height));
        int newHeight = (int) (Math.abs(Math.cos(radians) * height) + Math.abs(Math.sin(radians) * width));
    
        BufferedImage rotatedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = rotatedImage.createGraphics();
    
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    
        AffineTransform at = new AffineTransform();
        at.translate(newWidth / 2, newHeight / 2);
        at.rotate(radians);
        at.translate(-width / 2, -height / 2);
        g2d.drawImage(image, at, null);
    
        g2d.dispose();
        return rotatedImage;
    }
}
