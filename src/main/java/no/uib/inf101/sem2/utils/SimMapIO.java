package no.uib.inf101.sem2.utils;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import no.uib.inf101.sem2.model.SimMap;
import no.uib.inf101.sem2.entities.Colony;

public class SimMapIO {
    private static final String MAPS_DIRECTORY = "src/main/resources/maps";

    /**
     * Saves a SimMap instance to a JSON file.
     * The JSON file will be saved in the "src/main/json/maps/" directory.
     * If the directory does not exist, it will be created.
     *
     * @param map The SimMap instance to be saved.
     * @param fileName The name of the JSON file to save the map to.
     * @return True if the map was successfully saved, false otherwise.
     */
    public static boolean saveMap(SimMap map, String fileName) {
        Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(map);

        // Create the directories if they do not exist
        Path dirPath = Paths.get("src/main/resources/", "maps");
        try {
            Files.createDirectories(dirPath);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        try (FileWriter file = new FileWriter(dirPath.resolve(fileName).toString())) {
            file.write(json);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static String[] getAvailableMaps() {
        File mapsDir = new File(MAPS_DIRECTORY);
        if (!mapsDir.exists()) {
            return null;
        }
        return mapsDir.list();
    }

    /**
     * Loads a SimMap instance from a JSON file.
     * The method expects the JSON file to be located in the "src/main/resources/maps/" directory.
     * If the file is not found or there is an issue reading the file, this method will return null.
     * <p> Uses the {@link Colony.ColonyDeserializer} class to deserialize the colonies in the map.
     * <p> The method will also set the {@link SimMap#setMapFile(String)} property to the name of the file.
     *
     * @param filename The name of the JSON file containing the map data.
     * @return A SimMap instance populated with the data from the JSON file, or null if the file could not be read.
     */
    public static SimMap loadMap(String filename) {
        try (FileReader reader = new FileReader(new File(MAPS_DIRECTORY, filename))) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Colony.class, new Colony.ColonyDeserializer());
            Gson gson = gsonBuilder.create();
            
            SimMap simMap = gson.fromJson(reader, SimMap.class);
            return simMap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
