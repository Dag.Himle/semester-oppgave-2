package no.uib.inf101.sem2.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public class Rotater {

    /**
     * Rotates an image by the specified angle.
     * @param image The image to rotate
     * @param angle The angle to rotate the image by
     * @return The rotated image
     */
    public static BufferedImage imageRotate(Image image, double angle) {
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        double radians = Math.toRadians(angle);

        // Create a new BufferedImage with the same type as the original image
        BufferedImage rotatedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        // Create a Graphics2D object from the new BufferedImage
        Graphics2D g2d = rotatedImage.createGraphics();

        // Set rendering hints for better quality
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        // Perform the rotation using an AffineTransform and draw the original image onto the new BufferedImage
        AffineTransform at = new AffineTransform();
        at.translate(width / 2, height / 2);
        at.rotate(radians);
        at.translate(-width / 2, -height / 2);
        g2d.drawImage(image, at, null);

        // Dispose the Graphics2D object and return the rotated image
        g2d.dispose();
        return rotatedImage;
    }
}

