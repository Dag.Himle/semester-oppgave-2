package no.uib.inf101.sem2.utils.records;

/**
 * A record class for storing a position and a direction.
 * 
 * @param position The position as a {@link Position} object.
 * @param direction The direction as a double.
 */
public record PositionDirection(Position position, double direction) {
    public PositionDirection {
        if (direction < 0 || direction > 360) {
            throw new IllegalArgumentException("Direction must be between 0 and 360");
        }
    }

    /**
     * Get the x coordinate of the position.
     * 
     * @return The x coordinate.
     */
    public int x() {
        return position.x();
    }
    
    /**
     * Get the y coordinate of the position.
     * 
     * @return The y coordinate.
     */
    public int y() {
        return position.y();
    }
    
    /**
     * Get the position.
     * 
     * @return The position.
     */
    public Position getPosition() {
        return position;
    }
}
