package no.uib.inf101.sem2.utils;

import no.uib.inf101.sem2.entities.Ant;
import no.uib.inf101.sem2.entities.Colony;
import no.uib.inf101.sem2.entities.Food;
import no.uib.inf101.sem2.entities.Pheromone;
import no.uib.inf101.sem2.utils.records.Position;
import no.uib.inf101.sem2.utils.VectorUtils;
import no.uib.inf101.sem2.model.PheromoneGrid;

import java.util.List;



/**
 * A class that contains methods for sensing the environment
 * The ants use pheromones to find food and to communicate with each other.
 * By following trails of pheromones, ants can find food and return to their colony.
 * 
 * @author Dag O.B.H.  
 */
public class AntSense {

     /**
     * Update the ant's direction based on the pheromone trails in the environment.
     * If the ant is carrying food, it will follow TO_HOME pheromones; otherwise, it will follow TO_FOOD pheromones.
     * The ant's direction will be updated to point towards the weakest pheromone trail in its search radius.
     * If no pheromones are found, the ant's direction will not be changed.
     * The ants follow the weakest pheromone trail. As the weakest pheromone trail is the oldest,
     * and therefore the most likely to lead to the food source or colony.
     *
     * @param ant The ant whose direction needs to be updated.
     * @param grid The PheromoneGrid representing the environment.
     * @param searchRadius The radius around the ant's position in which to search for pheromones.
     * @return The direction of the strongest pheromone trail in the search radius, or the ant's current direction if no pheromones are found.
     * @author Dag O.B.H. 
     */
    public static double getPheromoneDirectionLowestStrength(Ant ant, PheromoneGrid grid, double searchRadius, List<Colony> colonies, List<Food> foods) {
        Pheromone.Type targetType = ant.isCarryingFood() ? Pheromone.Type.TO_HOME : Pheromone.Type.TO_FOOD;
        List<Pheromone> nearbyPheromones = grid.getPheromonesInRadius(ant.getCenter(), searchRadius);
    
        for (Colony colony : colonies) {
            if (colony.getColorString().equals(ant.getColonyColor())) {
                if (ant.isCarryingFood() && colony.getCenter().distance(ant.getCenter()) < ant.getRadius() + searchRadius + colony.getRadius()) {
                    return ant.getCenter().directionTo(colony.getCenter());
                }
            }
        }
        for (Food food : foods) {
            if (!ant.isCarryingFood() && food.getCenter().distance(ant.getCenter()) < ant.getRadius() + searchRadius + food.getRadius()) {
                return ant.getCenter().directionTo(food.getCenter());
            }
        }
        if (!nearbyPheromones.isEmpty()) {
            Pheromone weakestPheromone = null;
            double weakestStrength = 0;
            Position aggregatePosition = new Position(0, 0);
            int aggregateCount = 0;
    
            for (Pheromone pheromone : nearbyPheromones) {
                if (pheromone.getType() == targetType && ant.getColonyColor() == pheromone.getColonyColor()) {
                    if (weakestPheromone == null || pheromone.getStrength() < weakestStrength) {
                        weakestPheromone = pheromone;
                        weakestStrength = pheromone.getStrength();
                    }
                    aggregatePosition = VectorUtils.add(aggregatePosition, pheromone.getCenter());
                    aggregateCount++;
                }
            }
            if (nearbyPheromones.size() > 0) {
                if (weakestPheromone != null) {
                    double pheromoneDirection = ant.getCenter().directionTo(weakestPheromone.getCenter());
                    return ant.getDirection() * 0.4 + pheromoneDirection * 0.6;
                }
            } else {
                if (aggregateCount > 0) {
                    Position averagePosition = new Position(aggregatePosition.x() / aggregateCount, aggregatePosition.y() / aggregateCount);
                    double aggregateDirection = ant.getCenter().directionTo(averagePosition);
                    return ant.getDirection() * 0.4 + aggregateDirection * 0.6;
                }
            }
        }
        return ant.getDirection();
    }
    
}
