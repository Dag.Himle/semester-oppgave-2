package no.uib.inf101.sem2.utils.records;

import com.google.gson.annotations.Expose;

/**
 * A coordinate in a 2D plane
 * 
 * @param x The x coordinate
 * @param y the y coordinate
 * @throws IllegalArgumentException if either x or y is negative
 */

public record Position(
    @Expose int x, 
    @Expose int y) {

    public Position {
        if (x < 0 || y < 0) {
        //throw new IllegalArgumentException("Coordinates must be non-negative");
        }
    }

    /**
     * Calculate the distance between this position and another
     * 
     * @param targetPosition Position to calculate the distance to
     * @return The distance between the two positions
     */
    public double distance(Position targetPosition) {
        int dx = targetPosition.x() - x;
        int dy = targetPosition.y() - y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    /**
     * Calculate the direction from this position to another
     * <p>The direction is the angle in degrees between the x-axis and the line from this position to the target position.
     * The coordinate system used is the same as the one used in the simulation, with the y-axis pointing downwards and 
     * 0 degrees being the direction upwards/to the north.
     * <p>For example, if the target position is to the right of this position, the direction will be 90 degrees.
     * <p> The direction is rounded to 5 decimal places. To get the direction in radians, use {@link Math#toRadians(double)}.
     * @param targetPosition Position to calculate the direction to
     * @return The direction from this position to the target position
     */
    public double directionTo(Position targetPosition) {
        double angle = Math.atan2(targetPosition.y() - this.y(), targetPosition.x() - this.x());
        angle = Math.toDegrees(angle);
        angle = ((angle + 450) % 360); // Correct the angle to match the coordinate system of the simulation.
        double scale = Math.pow(10, 5);
        angle = Math.round(angle * scale) / scale;
        return angle;
    }
}  
