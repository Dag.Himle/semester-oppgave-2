package no.uib.inf101.sem2.utils.records;

import com.google.gson.annotations.Expose;


public record MapDimension(
    @Expose int width,
    @Expose int height
) {
    
}
