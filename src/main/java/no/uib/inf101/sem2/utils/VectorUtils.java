package no.uib.inf101.sem2.utils;


import no.uib.inf101.sem2.utils.records.Position;

/**
 * A class that contains static methods for vector operations
 */
public class VectorUtils {



    /**
     * Returns the magnitude of a vector
     *
     * @param vector
     * @return
     */
    public static Position normalize(Position vector) {
        double magnitude = Math.sqrt(vector.x() * vector.x() + vector.y() * vector.y());
        return new Position((int) (vector.x() / magnitude), (int) (vector.y() / magnitude));
    }

    /**
     * Returns the sum of two vectors
     *
     * @param point1
     * @param point2
     * @return
     */
    public static Position add(Position vector1, Position vector2) {
        return new Position(vector1.x() + vector2.x(), vector1.y() + vector2.y());
    }

    /**
     * Returns the difference between two vectors
     * @param vector1
     * @param vector2
     * @return
     */
    public static Position subtract(Position vector1, Position vector2) {
        return new Position(vector1.x() - vector2.x(), vector1.y() - vector2.y());
    }

    /**
     * Devides a vector by another vector
     * @param vector1
     * @param vector2
     * @return
     */
    public static Position divide(Position vector1, Position vector2) {
        return new Position(vector1.x() / vector2.x(), vector1.y() / vector2.y());
    }

    /**
     * Returns the dot product of two vectors
     *
     * @param vector1
     * @param vector2
     * @return
     */
    public static double dot(Position vector1, Position vector2) {
        return vector1.x() * vector2.x() + vector1.y() * vector2.y();
    }

    /**
     * Returns the scalar product of a vector and a scalar
     * @param vector
     * @param scalar
     * @return
     */
    public static Position scale(Position vector, double scalar) {
        return new Position((int) (vector.x() * scalar),(int) (vector.y() * scalar));
    }
}