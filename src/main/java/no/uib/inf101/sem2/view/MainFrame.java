package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.model.AntSimulation;
import no.uib.inf101.sem2.model.SimMap;
import no.uib.inf101.sem2.model.SimulationState;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainFrame extends JFrame {
    private static final String MENU_SCREEN = "menu";
    private static final String SIMULATION_SCREEN = "simulation";
    private static final String SCENARIO_SCREEN = "scenario";
    private static final String CREATE_MAP_SCREEN = "createMap";

    private CardLayout cardLayout;
    private JPanel mainPanel;

    public MainFrame() {
        

        setTitle("Ant Simulation");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));
        pack();

        cardLayout = new CardLayout();
        mainPanel = new JPanel(cardLayout);

        // Add the simulation screen
        AntSimulation simulation = new AntSimulation();
        AntSimulationPanel antSimulationPanel = new AntSimulationPanel(simulation, () -> {
            cardLayout.show(mainPanel, SCENARIO_SCREEN);
            simulation.state = SimulationState.SCENARIO;
        });
        simulation.setView(antSimulationPanel);
        
        mainPanel.add(antSimulationPanel, SIMULATION_SCREEN);
        simulation.start();

        // Create the menu screen and add a listener for the start button
        MenuScreen menuScreen = new MenuScreen(e -> {
            cardLayout.show(mainPanel, SCENARIO_SCREEN);
            simulation.state = SimulationState.SCENARIO;
        });
        mainPanel.add(menuScreen, MENU_SCREEN);

        // Create the scenario screen and add a listener for the start button
        // Rezie the window to the size of the map + space for buttons
        ScenarioPanel scenarioScreen = new ScenarioPanel(simulation, () -> {
            simulation.state = SimulationState.RUNNING;
            SimMap map = simulation.getMap();
            setPreferredSize(new Dimension(map.getWidth() + 250, map.getHeight()+45));
            setSize(map.getWidth() + 250, map.getHeight()+45);
            pack();
            cardLayout.show(mainPanel, SIMULATION_SCREEN);
        });
        mainPanel.add(scenarioScreen, SCENARIO_SCREEN);

        // Create the create map screen and add a listener for the return button
        CreateMapPanel createMapScreen = new CreateMapPanel(simulation, scenarioScreen, () -> {
            cardLayout.show(mainPanel, SCENARIO_SCREEN);
        });
        mainPanel.add(createMapScreen, CREATE_MAP_SCREEN);

        

        // Show the menu screen by default
        cardLayout.show(mainPanel, MENU_SCREEN);

        add(mainPanel);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                simulation.shutdown();
            }
        });
    }
}
