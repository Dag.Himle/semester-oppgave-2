package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.model.SimMap;
import no.uib.inf101.sem2.model.AntSimulation;
import no.uib.inf101.sem2.view.components.MapPreviewComponent;


import javax.swing.*;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.List;

public class ScenarioPanel extends JPanel {

    // Data fields
    private AntSimulation simulation;
    private SimMap map;

    // Components
    private JComponent mapPreviewComponent;
    private JComboBox<String> mapSelector; 



    public ScenarioPanel(AntSimulation simulation, Runnable onStartSimulation) {
        this.simulation = simulation;
        this.map = simulation.getMap();
        this.mapPreviewComponent = new MapPreviewComponent(map, true);
        this.mapSelector = new JComboBox<>();

        this.setSize(400, 400);
    
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        Insets insets = new Insets(10, 10, 10, 10);
    
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.insets = insets;
        gbc.fill = GridBagConstraints.BOTH;
        add(mapPreviewComponent, gbc);
    
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0;
        gbc.weighty = 0;
        gbc.fill = GridBagConstraints.NONE;
        add(createMapButtonsPanel(), gbc);
    
        gbc.gridy = 1;
        gbc.gridx = 0;
        gbc.gridwidth = 2;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.CENTER;
        add(createSettingsPanel(), gbc);
    
        gbc.gridy = 2;
        add(createStartSimulationButton(onStartSimulation), gbc);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Rectangle rect = new Rectangle(0, getHeight()-40 , getWidth(), getHeight());
        g.setColor(Color.DARK_GRAY);
        g.fillRect(rect.x, rect.y, rect.width, rect.height);
        updateMapPanel();
    }

    private void updateMapPanel() {
        ((MapPreviewComponent) mapPreviewComponent).setMap(map);
        mapPreviewComponent.repaint();
    }

    private JPanel createMapButtonsPanel() {
        JLabel mapLabel = new JLabel("Map:");
        populateMapSelector();
        mapSelector.addActionListener(e -> {
            if (mapSelector.getSelectedItem() != null) {
                String selectedMap = (String) mapSelector.getSelectedItem().toString();
                loadMap(selectedMap);
            }
        });

        JButton saveMapButton = new JButton("Manage Maps");
        saveMapButton.addActionListener(e -> {
            ((CardLayout) getParent().getLayout()).show(getParent(), "createMap");
            loadMap(mapSelector.getSelectedItem().toString());
        });

        GridLayout layout = new GridLayout(3, 1);
        layout.setVgap(10);
        JPanel mapButtonsPanel = new JPanel(layout);
        mapButtonsPanel.add(mapLabel);
        mapButtonsPanel.add(mapSelector);
        mapButtonsPanel.add(saveMapButton);

        return mapButtonsPanel;
    }

    private JPanel createSettingsPanel() {
        JButton loadSettingsButton = new JButton("Load Settings");
        loadSettingsButton.addActionListener(e -> {
            JOptionPane.showMessageDialog(this, "Not implemented yet, sorry!");
        });

        JButton saveSettingsButton = new JButton("Save Settings");
        saveSettingsButton.addActionListener(e -> {
            JOptionPane.showMessageDialog(this, "Not implemented yet, sorry!");
        });

        JPanel settingsButtonsPanel = new JPanel(new GridLayout(1, 2));
        settingsButtonsPanel.add(loadSettingsButton);
        settingsButtonsPanel.add(saveSettingsButton);

        return settingsButtonsPanel;
    }

    private JButton createStartSimulationButton(Runnable onStartSimulation) {
        JButton startSimulationButton = new JButton("Start Simulation");
        startSimulationButton.addActionListener(e -> onStartSimulation.run());
        return startSimulationButton;
    }
    
    private List<String> loadAvailableMaps() {
        Path mapsPath = Paths.get("src/main/resources/maps/");
        List<String> maps = new ArrayList<>();
        try {
            maps = Files.list(mapsPath)
                    .filter(Files::isRegularFile)
                    .map(path -> {
                        String fileName = path.getFileName().toString();
                        if (fileName.endsWith(".json")) {
                            return fileName.substring(0, fileName.length() - 5);
                        }
                        return fileName;
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return maps;
    }

    private void populateMapSelector() {
        loadAvailableMaps().forEach(mapSelector::addItem);
    }

    public void updateMapList() {
        mapSelector.removeAllItems();
        loadAvailableMaps().forEach(mapSelector::addItem);
    }



    private void loadMap(String mapName) {
        // Load the selected map in the simulation
        simulation.loadMap(mapName+".json");
        map = simulation.getMap();
        updateMapPanel();
    }
}



