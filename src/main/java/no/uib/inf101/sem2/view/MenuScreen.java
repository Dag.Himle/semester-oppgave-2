package no.uib.inf101.sem2.view;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MenuScreen extends JPanel {


    public MenuScreen(ActionListener startSimulationListener) {
        setLayout(new BorderLayout());
        
        // Title
        JLabel titleLabel = new JLabel("Ant Simulation", SwingConstants.CENTER);
        titleLabel.setFont(new Font("Arial", Font.BOLD, 32));
        add(titleLabel, BorderLayout.NORTH);
        
        // Buttons
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        // Start button
        JButton startButton = new JButton("Start");
        startButton.setFont(new Font("Arial", Font.PLAIN, 18));
        startButton.addActionListener(startSimulationListener);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10);
        buttonPanel.add(startButton, gbc);
        
        // Info button
        JButton infoButton = new JButton("Info");
        infoButton.setFont(new Font("Arial", Font.PLAIN, 18));
        // Add action listener for info button here
        gbc.gridx = 0;
        gbc.gridy = 1;
        buttonPanel.add(infoButton, gbc);
        
        // Settings button
        JButton settingsButton = new JButton("Settings");
        settingsButton.setFont(new Font("Arial", Font.PLAIN, 18));
        // Add action listener for settings button here
        gbc.gridx = 0;
        gbc.gridy = 2;
        buttonPanel.add(settingsButton, gbc);
        
        add(buttonPanel, BorderLayout.CENTER);
    }
}

