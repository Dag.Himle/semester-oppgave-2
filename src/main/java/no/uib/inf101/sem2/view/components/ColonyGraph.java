package no.uib.inf101.sem2.view.components;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ColonyGraph extends JComponent {

    // Constants
    private static final int GRAPH_WIDTH = 200;
    private static final int PADDING = 10;
    private static final int MAX_POINTS = 50;
    private static final int MOVING_AVERAGE_WINDOW = 10;
    private static final int UPDATE_FREQUENCY = 10;

    // Variables
    private int height = 0;
    
    //Counter 
    private int updateCounter = 0;
    
    private String colonyColor;
    private List<Integer> antCounts;
    private List<Integer> movingAverageAntCounts;

    public ColonyGraph(String colonyColor, int height) {
        this.colonyColor = colonyColor;
        this.antCounts = new ArrayList<>();
        this.movingAverageAntCounts = new ArrayList<>();
        this.height = height;
        setPreferredSize(new Dimension(GRAPH_WIDTH + 2 * PADDING, height + 2 * PADDING));
    }

    public void addAntCount(int antCount) {
        updateCounter++;
        if (updateCounter % UPDATE_FREQUENCY != 0) {
            return;
        }
        if (antCounts.size() >= MAX_POINTS) {
            antCounts.remove(0);
        }
        antCounts.add(antCount);
        
        if (movingAverageAntCounts.size() >= MOVING_AVERAGE_WINDOW) {
            movingAverageAntCounts.remove(0);
        }
        movingAverageAntCounts.add(antCount);
        double average = movingAverageAntCounts.stream().mapToDouble(Integer::doubleValue).average().orElse(0);
        
        if (antCounts.size() >= MAX_POINTS) {
            antCounts.set(antCounts.size() - 1, (int) average);
        } else {
            antCounts.add((int) average);
        }
    
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        setBackground(Color.BLACK);

        // Draw title
        g.setColor(getColor(colonyColor));
        if (antCounts.isEmpty()) {
            g.drawString(colonyColor + ": 0", PADDING, PADDING);
        } else {  
            g.drawString(colonyColor+ ":" + antCounts.get(antCounts.size() -1), PADDING, PADDING);
        }

        // Draw graph
        g.setColor(Color.BLACK);
        g.fillRect(PADDING, PADDING * 2, GRAPH_WIDTH, height);

        // Draw graph lines
        if (!antCounts.isEmpty()) {
            g.setColor(getColor(colonyColor));
            int maxAntCount = antCounts.stream().max(Integer::compare).orElse(0);
            double scaleX = (double) GRAPH_WIDTH / (antCounts.size() - 1);
            double scaleY = maxAntCount > 0 ? (double) height / maxAntCount : 1;
        
            int previousX = PADDING;
            int previousY = PADDING * 2 + height - (int) (antCounts.get(0) * scaleY);
        
            for (int i = 1; i < antCounts.size(); i++) {
                int x = PADDING + (int) (i * scaleX);
                int y = PADDING * 2 + height - (int) (antCounts.get(i) * scaleY);
        
                g.drawLine(previousX, previousY, x, y);
        
                previousX = x;
                previousY = y;
            }
        }
    }

    /**
     * Returns the color of the colony.
     * @return
     */
    public String getColonyColor() {
        return colonyColor;
    }

    private Color getColor(String color) {
        switch (color) {
            case "red":
                return Color.RED;
            case "blue":
                return Color.BLUE;
            case "yellow":
                return Color.YELLOW;
            case "cyan":
                return Color.CYAN;
            default:
                return Color.BLACK;
        }
    }
}