package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.entities.*;
import no.uib.inf101.sem2.model.AntSimulation;
import no.uib.inf101.sem2.utils.records.*;
import no.uib.inf101.sem2.view.components.ColonyGraph;
import no.uib.inf101.sem2.utils.RotatedImageIcon;

import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class AntSimulationPanel extends JPanel {

    private AntSimulation simulation;

    // Components
    private List<ColonyGraph> colonyGraphs; 
    private JPanel statsPanel;
    private JPanel sidePanel;




    /**
     * Constructor for AntSimulationPanel
     * The constructor creates the panel and adds the components to it
     * @param simulation
     */
    public AntSimulationPanel(AntSimulation simulation, Runnable returnToScenario) {
        this.simulation = simulation;

        this.colonyGraphs = new ArrayList<>();
        this.setBackground(Color.BLACK);

        BorderLayout mainLayout = new BorderLayout(10, 20);
        setLayout(mainLayout);
        setBorder(new EmptyBorder(5, 5, 5, 5));
        
        
        AntSimulationComponent antSimulationComponent = new AntSimulationComponent();
        add(antSimulationComponent, BorderLayout.CENTER);

        this.sidePanel = new JPanel();
        sidePanel.setBackground(Color.BLACK);
        BorderLayout layout = new BorderLayout(10 , 10);
        sidePanel.setLayout(layout);

        this.statsPanel = createStatsPanel();

        sidePanel.add(statsPanel, BorderLayout.NORTH);

        JPanel buttonsPanel = createButtonsPanel(returnToScenario);
        sidePanel.add(buttonsPanel, BorderLayout.SOUTH);
        sidePanel.setBorder(getBorder());
        add(sidePanel, BorderLayout.EAST);


    }

    private JPanel createStatsPanel() {
        JPanel statsPanel = new JPanel();
        statsPanel.setBackground(Color.BLACK);
        statsPanel.setLayout(new GridBagLayout());
    
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 0.5;
        gbc.weighty = 0.0;
        String[] colonyColors = {};
        if (simulation.getMap().getColonies().size() > 0) {
            Set<String> uniqueColors = new HashSet<>(); // Create a Set to store unique colors
        
            for (Colony colony : simulation.getMap().getColonies()) {
                String color = colony.getColorString();
                uniqueColors.add(color); // Add the color to the set, duplicates will be ignored
            }
            colonyColors = uniqueColors.toArray(new String[0]); // Convert the set to an array
        }
            for (int i = 0; i < colonyColors.length; i++) {
                System.out.println(colonyColors[i]);
            }
    
        MapDimension mapDimension = simulation.getMap().getSize();
        for (int i = 0; i < colonyColors.length; i++) {
            gbc.gridy = i;
            ColonyGraph colonyGraph = new ColonyGraph(colonyColors[i], mapDimension.height()/colonyColors.length - 100 );
            colonyGraphs.add(colonyGraph); 
            statsPanel.add(colonyGraph, gbc);
        }
        return statsPanel;
    }

    private JPanel createButtonsPanel(Runnable returnToScenario){
        JPanel buttonsPanel = new JPanel();

        buttonsPanel.setBackground(Color.BLACK);
        buttonsPanel.setLayout(new GridBagLayout());

        GridBagConstraints gbc2 = new GridBagConstraints();
        gbc2.gridx = 0;
        gbc2.gridy = 0;
        JButton pauseButton = new JButton("Pause");
        pauseButton.addActionListener(e -> {
            simulation.setPaused(!simulation.isPaused());
            pauseButton.setText(simulation.isPaused() ? "Resume" : "Pause");
        });
        buttonsPanel.add(pauseButton, gbc2);

        gbc2.gridy = 0;
        gbc2.gridx = 1;
        JButton stepButton = new JButton("Step");
        stepButton.addActionListener(e -> {
            simulation.step(true);
        });
        buttonsPanel.add(stepButton, gbc2);

        gbc2.gridy = 1;
        gbc2.gridx = 0;
        gbc2.gridwidth = 2;
        JButton backButton = new JButton("Back to Scenario");
        backButton.addActionListener(e -> {
            returnToScenario.run();
        });
        buttonsPanel.add(backButton, gbc2);


        return buttonsPanel;
    }

    public void updateStatsPanel() {
        // Remove the old statsPanel
        for (Component component : sidePanel.getComponents() ) {
            if (component == statsPanel) {
                sidePanel.remove(component);
            }
        }
        colonyGraphs.clear();
    
        // Re-create the statsPanel
        this.statsPanel = createStatsPanel();
        sidePanel.add(statsPanel, BorderLayout.NORTH);

        // Update the UI
        revalidate();
        repaint();
    }

    
    class AntSimulationComponent extends JPanel {

        @Override
        protected void paintComponent(Graphics g) {
            setBackground(Color.BLACK);
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;

            g2d.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setRenderingHint(java.awt.RenderingHints.KEY_INTERPOLATION, java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR);

            g2d.setColor(java.awt.Color.WHITE);
            g2d.drawRect(0, 0, simulation.getMap().getSize().width() - 1, simulation.getMap().getSize().height() - 1);
            
            HashMap<String, Integer> antCount = simulation.getAntCount();
            for (int i = 0; i < colonyGraphs.size(); i++) {
                Integer count = antCount.get(colonyGraphs.get(i).getColonyColor());
                if (count == null) {
                    count = 0;
                }
                colonyGraphs.get(i).addAntCount(count);
            }
            drawEntities(g2d);
            drawAnts(g2d);
        }


        @Override
        public Dimension getPreferredSize() {
            int totalWidth = simulation.getMap().getSize().width() + 200;
            int totalHeight = simulation.getMap().getSize().height();
            return new Dimension(totalWidth, totalHeight);
        }


        private void drawEntities(Graphics2D g2d) {
            for (IDrawableChar entity : simulation.getDrawableEntities()) {
                Position position = entity.getPosition();
                int radius = entity.getRadius();

                if (entity instanceof Pheromone || entity instanceof Food) {
                    g2d.setColor(entity.getColor());
                    g2d.fillOval((int) position.x(), (int) position.y(), 2 * radius, 2 *radius);
                } else if (entity instanceof Wall) {
                    g2d.setColor(entity.getColor());
                    g2d.fillRect((int) position.x(), (int) position.y(), 2 * radius, 2 * radius);
                } else if (entity instanceof Colony) {
                    g2d.setColor(entity.getColor());
                    g2d.fillOval((int) position.x(), (int) position.y(), 2 * radius, 2* radius);
                    
                }
            }
        }
        

        private void drawAnts(Graphics2D g2d){
            for (Colony colony : simulation.getColonies()) {
                for (Ant ant : colony.getAnts()) {
                    RotatedImageIcon sprite = colony.getRotatedSprite((int) ant.getDirection());
                    Image image = sprite.getImage();
                    g2d.drawImage(image, (int) ant.getPosition().x(), (int) ant.getPosition().y(), null);
                } 
            }
        }
    } 
}
