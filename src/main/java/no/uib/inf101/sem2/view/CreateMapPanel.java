package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.model.AntSimulation;
import no.uib.inf101.sem2.model.SimMap;
import no.uib.inf101.sem2.utils.SimMapIO;
import no.uib.inf101.sem2.utils.records.Position;
import no.uib.inf101.sem2.view.components.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

public class CreateMapPanel extends JPanel {

    enum Mode {
        NORMAL,
        DELETE,
        ADD_FOOD,
        ADD_COLONY,
        ADD_WALL
    }

    //Helper fields:
    private Mode currentMode = Mode.NORMAL;
    private Point lastWallPoint = null;
    private int wallSize = 0;
    private final String MAPS_FOLDER = "src/main/resources/maps/";

    // Data fields:
    private AntSimulation simulation;
    private ScenarioPanel scenarioPanel;
    private SimMap map;

    //Components:
    private MapPreviewComponent mapPreviewComponent;



    public CreateMapPanel(AntSimulation simulation, ScenarioPanel scenarioPanel, Runnable onReturnToScenario) {
        this.simulation = simulation;
        this.map = simulation.getMap();
        this.scenarioPanel = scenarioPanel;
        this.mapPreviewComponent = new MapPreviewComponent(map, true);

        BorderLayout layout = new BorderLayout();
        setLayout(layout);

        // Add map preview component:
        add(mapPreviewComponent, BorderLayout.CENTER);

        // Add buttons panel:
        JComponent buttonsPanel = createButtonsPanel(onReturnToScenario);
        add(buttonsPanel, BorderLayout.SOUTH);

        mapPreviewComponent.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                switch (currentMode) {
                    case ADD_COLONY:
                        ColonyParameters colonyParams = showColonyParametersDialog(e.getPoint());
                        if (colonyParams != null) {
                            map.addColony(colonyParams.position, colonyParams.size, colonyParams.color);
                            mapPreviewComponent.repaint();
                        }
                        currentMode = Mode.NORMAL;
                        break;
                    case ADD_FOOD:
                        handleAddFoodMode(e);
                    case DELETE:
                        handleDeleteMode(e);
                }
            }
         });
        
        mapPreviewComponent.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (currentMode == Mode.ADD_WALL) {
                    handleAddWallMode(e);
                } else if (currentMode == Mode.DELETE) {
                    handleDeleteMode(e);
                }
            }
        });

    }   

    private JPanel createButtonsPanel(Runnable onReturnToScenario) {
        JPanel buttonsPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        
        JButton deleteButton = new JButton("Delete");
        deleteButton.addActionListener(e -> {
            currentMode = Mode.DELETE;
        });
        gbc.gridx = 0;
        gbc.gridy = 0;
        buttonsPanel.add(deleteButton, gbc);

        JButton addColonyButton = new JButton("Add Colony");
        addColonyButton.addActionListener(e -> {
            currentMode = Mode.ADD_COLONY;
        });
        gbc.gridx = 1;
        buttonsPanel.add(addColonyButton, gbc);

        JButton addWallButton = new JButton("Draw Walls");
        addWallButton.addActionListener(e -> {
            setWallDrawSize();
        });
        gbc.gridx = 2;
        buttonsPanel.add(addWallButton, gbc);

        JButton addFoodButton = new JButton("Add Food");
        addFoodButton.addActionListener(e -> {
            currentMode = Mode.ADD_FOOD;
        });
        gbc.gridx = 3;
        buttonsPanel.add(addFoodButton, gbc);

        JButton newMapButton = new JButton("New Map");
        newMapButton.addActionListener(e -> {
            createNewMap();
        });
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1;
        buttonsPanel.add(newMapButton, gbc);
        
        JButton saveMapButton = new JButton("Save Map");
        saveMapButton.addActionListener(e -> {
            if (saveMap()) {
                onReturnToScenario.run();
            }
        });
        gbc.gridx = 1;
        buttonsPanel.add(saveMapButton, gbc);

        JButton loadMapButton = new JButton("Load Map");
        loadMapButton.addActionListener(e -> {
            loadMap();
        });
        gbc.gridx = 2;
        buttonsPanel.add(loadMapButton, gbc);

        JButton returnButton = new JButton("Return to Scenario");
        returnButton.setBackground(Color.BLUE);
        returnButton.setForeground(Color.BLUE.brighter());
        returnButton.addActionListener(e -> {
            int savechoice = JOptionPane.showConfirmDialog(CreateMapPanel.this, "Do you want to save the map?",
                         "Unsaved Map Warning", JOptionPane.INFORMATION_MESSAGE);
            if (savechoice == JOptionPane.YES_OPTION) {
                saveMap();
                onReturnToScenario.run();
            } else if (savechoice == JOptionPane.NO_OPTION) {
                onReturnToScenario.run();
            }

        });
        gbc.gridx = 3;
        buttonsPanel.add(returnButton, gbc);

        return buttonsPanel;
    }

    private Point getMapCoordinates(MouseEvent e) {
        double scaleX;
        double scaleY;
        int x = 0;
        int y = 0;
        if (map.getSize().width() > mapPreviewComponent.getWidth()){
            scaleX = (double) mapPreviewComponent.getWidth() / map.getSize().width();
            x = (int) (e.getX() / scaleX);
        } else {
            scaleX = (double) map.getSize().width() / mapPreviewComponent.getWidth();
            x = (int) (e.getX() * scaleX);
        }
        if (map.getSize().height() > mapPreviewComponent.getHeight()){
            scaleY = (double) mapPreviewComponent.getHeight() / map.getSize().height();
            y = (int) (e.getY() / scaleY);
        } else {
            scaleY = (double) map.getSize().height() / mapPreviewComponent.getHeight();
            y = (int) (e.getY() * scaleY);
        }


        return new Point(x, y);
    }

    private ColonyParameters showColonyParametersDialog(Point clickPosition) {
        JPanel panel = new JPanel(new GridLayout(0, 2));
    
        JLabel sizeLabel = new JLabel("Size:");
        JTextField sizeField = new JTextField();
        panel.add(sizeLabel);
        panel.add(sizeField);
    
        JLabel colorLabel = new JLabel("Color:");
        String[] colorOptions = {"red", "blue", "yellow", "cyan"};
        JComboBox<String> colorComboBox = new JComboBox<>(colorOptions);
        panel.add(colorLabel);
        panel.add(colorComboBox);
    
        int result = JOptionPane.showConfirmDialog(this, panel, "Add Colony", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
    
        if (result == JOptionPane.OK_OPTION) {
            int size;
            try {
                size = Integer.parseInt(sizeField.getText());
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Invalid size. Please enter an integer.", "Error", JOptionPane.ERROR_MESSAGE);
                return null;
            }
    
            String color = (String) colorComboBox.getSelectedItem();
    
            return new ColonyParameters(clickPosition, size, color);
        } else {
            return null;
        }
    }
    
    private static class ColonyParameters {
        final Position position;
        final int size;
        final String color;
    
        public ColonyParameters(Point position, int size, String color) {
            if (size < 50){
                this.position = new Position((int) position.getX(), (int) position.getY());
            } else {
                this.position = new Position((int) position.getX(), (int) position.getY());
            }
            this.size = size;
            this.color = color;
        }
    }

    private void handleAddFoodMode(MouseEvent e) {
        Point mapCoordinates = getMapCoordinates(e);
        String foodAmountStr = JOptionPane.showInputDialog(CreateMapPanel.this, "Enter the amount of food:");
        if (foodAmountStr != null) {
            try {
                int foodAmount = Integer.parseInt(foodAmountStr);
                if (foodAmount >= 0) {
                    Position foodcenter = new Position((int) mapCoordinates.x - foodAmount/2,(int) mapCoordinates.y- foodAmount/2);
                    map.addFood(foodcenter, foodAmount);
                    mapPreviewComponent.repaint();
                } else {
                    JOptionPane.showMessageDialog(CreateMapPanel.this, "Food amount must be a positive number.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(CreateMapPanel.this, "Please enter a valid number.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
            }
        }
        currentMode = Mode.NORMAL;
    }

    private void handleAddWallMode(MouseEvent e) {
        Point mapCoordinates = getMapCoordinates(e);

        if (lastWallPoint == null || lastWallPoint.distance(mapCoordinates) >= wallSize/2) {
            Position wallCenter = new Position((int) mapCoordinates.x, (int) mapCoordinates.y);
            if (map.isPositionInBounds(wallCenter)) {
                map.addWall(wallCenter, wallSize);
                mapPreviewComponent.repaint();
                lastWallPoint = new Point(wallCenter.x(), wallCenter.y());
            }
        }
    }

    private void setWallDrawSize(){
        String wallSizeStr = JOptionPane.showInputDialog(CreateMapPanel.this, "Enter the size of the wall:");

        if (wallSizeStr != null) {
            try {
                int wallSize = Integer.parseInt(wallSizeStr);
                if (wallSize >= 0) {
                    if (wallSize > 0) {
                        lastWallPoint = null;
                        this.wallSize = wallSize;
                        currentMode = Mode.ADD_WALL;
                    } else {
                        JOptionPane.showMessageDialog(CreateMapPanel.this, "Wall size must be a positive number.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(CreateMapPanel.this, "Wall size must be a positive number.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(CreateMapPanel.this, "Please enter a valid number.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    private void handleDeleteMode(MouseEvent e) {
        Point mapCoordinates = getMapCoordinates(e);
        map.deleteObjectAt(new Position(mapCoordinates.x, mapCoordinates.y));
        mapPreviewComponent.repaint();
    }

    private void createNewMap() {
        JPanel panel = new JPanel(new GridLayout(0, 2));
    
        JLabel widthLabel = new JLabel("Width:");
        JTextField widthField = new JTextField();
        panel.add(widthLabel);
        panel.add(widthField);
    
        JLabel heightLabel = new JLabel("Height:");
        JTextField heightField = new JTextField();
        panel.add(heightLabel);
        panel.add(heightField);
    
        int result = JOptionPane.showConfirmDialog(this, panel, "New Map Dimensions", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
    
        if (result == JOptionPane.OK_OPTION) {
            try {
                int width = Integer.parseInt(widthField.getText());
                int height = Integer.parseInt(heightField.getText());
    
                if ( width < 1000 &&  width > 0 && height < 1000 && height > 0) {
                    map = new SimMap(width, height);
                    mapPreviewComponent.setMap(map);
                    mapPreviewComponent.repaint();
                } else {
                    JOptionPane.showMessageDialog(this, "Width and height must be positive number less than 1000.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Invalid dimensions. Please enter integers.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }


    private boolean saveMap() {
        String mapName;
        if (map.getName() == null || map.getName().trim().isEmpty()) {
            mapName = JOptionPane.showInputDialog(this, "Enter the map name:");
            if (mapName == null || mapName.trim().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Please enter a valid map name.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            map.setName(mapName);
        } else {
            mapName = map.getName();
        }
    
        boolean success = SimMapIO.saveMap(map, mapName + ".json");
        if (success) {
            JOptionPane.showMessageDialog(this, "Map saved successfully. Under name:" + mapName, "Save Successful", JOptionPane.INFORMATION_MESSAGE);
            simulation.loadMap(mapName + ".json");
            map = simulation.getMap();
            scenarioPanel.updateMapList();
            mapPreviewComponent.setMap(map);
            mapPreviewComponent.repaint();
            return true;
        } else {
            JOptionPane.showMessageDialog(this, "Error saving the map.", "Save Failed", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    private void loadMap() {
        File mapsFolder = new File(MAPS_FOLDER);
        FilenameFilter jsonFilter = (dir, name) -> name.endsWith(".json");
        File[] mapFiles = mapsFolder.listFiles(jsonFilter);
    
        if (mapFiles == null || mapFiles.length == 0) {
            JOptionPane.showMessageDialog(this, "No maps found in the resources/maps folder.", "No Maps", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
    
        String[] mapNames = Arrays.stream(mapFiles)
                .map(file -> file.getName().substring(0, file.getName().length() - 5))
                .toArray(String[]::new);
    
        String selectedMapName = (String) JOptionPane.showInputDialog(
                this,
                "Select a map to load:",
                "Load Map",
                JOptionPane.PLAIN_MESSAGE,
                null,
                mapNames,
                mapNames[0]
        );
    
        if (selectedMapName != null) {
            SimMap loadedMap = SimMapIO.loadMap(selectedMapName + ".json");
            if (loadedMap != null) {
                simulation.loadMap(selectedMapName + ".json");
                map = simulation.getMap();
                mapPreviewComponent.setMap(map);
                mapPreviewComponent.repaint();
            } else {
                JOptionPane.showMessageDialog(this, "Error loading the map.", "Load Failed", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}