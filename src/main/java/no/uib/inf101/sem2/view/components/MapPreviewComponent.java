package no.uib.inf101.sem2.view.components;

import javax.swing.JPanel;
import java.awt.*;

import no.uib.inf101.sem2.entities.IDrawableChar;
import no.uib.inf101.sem2.entities.Wall;
import no.uib.inf101.sem2.model.SimMap;

public class MapPreviewComponent extends JPanel {
    
    private SimMap map;
    private boolean include_details;

    public MapPreviewComponent(SimMap map, boolean include_details) {
        this.map = map;
        this.include_details = include_details;
        setPreferredSize(new Dimension(200, 200));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawMapPreview(g, map, 0, 0, getWidth(), getHeight());
    }

    public void setMap(SimMap map) {
        this.map = map;
    }

    private void drawMapPreview(Graphics g, SimMap map, int previewX, int previewY, int previewWidth, int previewHeight) {
        Iterable<IDrawableChar> drawables = map.getDrawables();
        int mapWidth = map.getSize().width();
        int mapHeight = map.getSize().height();
    
        double scaleX = (double) previewWidth / mapWidth;
        double scaleY = (double) previewHeight / mapHeight;
    
        g.setColor(Color.BLACK);
        g.fillRect(previewX, previewY, previewWidth, previewHeight);
    
        for (IDrawableChar drawable : drawables) {
            int x = drawable.getPosition().x();
            int y = drawable.getPosition().y();
            Color color = drawable.getColor();
    
            int scaledX = previewX + (int) (x * scaleX);
            int scaledY = previewY + (int) (y * scaleY);
            int radius = (int) (drawable.getRadius() * scaleX);
    
            g.setColor(color);
            if (drawable instanceof Wall) {
                g.fillRect(scaledX, scaledY, radius*2, radius*2);
            } else {
                g.fillOval(scaledX, scaledY, radius*2, radius*2);
            }
        }
        if (include_details) {
            drawMapPreviewDetails(g, map, previewX, previewY, previewWidth, previewHeight);
        }
    }

    private void drawMapPreviewDetails(Graphics g, SimMap map, int previewX, int previewY, int previewWidth, int previewHeight) {
        int mapWidth = map.getSize().width();
        int mapHeight = map.getSize().height();
        // Draw a border around the map preview
        g.setColor(Color.WHITE);
        g.drawRect(previewX, previewY, previewWidth, previewHeight);
    
        // Draw the map size in the bottom right corner
        g.setColor(Color.WHITE);
        String mapSize = mapWidth + "x" + mapHeight;
        int stringwidth = g.getFontMetrics().stringWidth(mapSize);
        g.drawString(mapSize, previewX + previewWidth - stringwidth - 10, previewY + previewHeight - 10);
    
        //Draw the map name in middle top of the map preview
        String mapName = map.getName();
        if (mapName == null) {
            mapName = "Unnamed map";
        }
        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.BOLD, 20));
        int mapNameWidth = g.getFontMetrics().stringWidth(mapName);
        int mapNameHeight = g.getFontMetrics().getHeight();
        g.drawString(mapName, previewX + previewWidth / 2 - mapNameWidth / 2, previewY + mapNameHeight);
    }
}


