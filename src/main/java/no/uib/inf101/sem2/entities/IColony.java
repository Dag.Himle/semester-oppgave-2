package no.uib.inf101.sem2.entities;


import no.uib.inf101.sem2.utils.RotatedImageIcon;

import java.util.Iterator;
import java.util.List;


public interface IColony<T> extends Iterable<Ant>{



    /**
     * Set the size of the colony.
     * @param size The new size of the colony.
     */
    public void setSize(int size);

    /**
     * @return An iterator over the ants in the colony.
     */
    public Iterator<Ant> iterator();

    /**
     * Add an ant to the colony.
     * @param ant The ant to add.
     */
    public void addAnt(Ant ant);

    /**
     * Get the ants in the colony.
     * @return A list of ants.
     */
    public List<Ant> getAnts();

    /**
     * Get rotated sprite for the ant with the given direction.
     * @param dir The direction of the ant.
     * @return The rotated sprite as a {@link RotatedImageIcon}.
     */
    public RotatedImageIcon getRotatedSprite(int rotation);

    
}
