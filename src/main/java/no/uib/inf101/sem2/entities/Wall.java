package no.uib.inf101.sem2.entities;

import java.awt.Color;

import com.google.gson.annotations.Expose;

import no.uib.inf101.sem2.utils.records.Position;

public class Wall implements IDrawableChar {

    @Expose
    private final Position CENTER;
    @Expose
    private int size;

    private static final Color WALL_COLOR = Color.GRAY;

    public Wall(Position position, int size) {
        this.CENTER = position;
        this.size = size;
    }

    @Override
    public Position getPosition() {
        return new Position(CENTER.x() - getRadius(), CENTER.y() - getRadius());
    }

    @Override
    public Position getCenter() {
        return CENTER;
        
    }

    @Override
    public Color getColor() {
        return WALL_COLOR;
    }

    @Override
    public Integer getRadius() {
        return size/2;
    }

    @Override
    public char getDrawType(){
        return 'W';
    }

    /**
     * Checks if the given position is within the wall
     * @param pos
     * @return
     */
    public boolean contains(Position pos) {
        return getCenter().distance(pos) < getRadius();
    }
}