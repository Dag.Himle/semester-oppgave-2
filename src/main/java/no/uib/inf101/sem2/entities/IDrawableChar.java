package no.uib.inf101.sem2.entities;

import no.uib.inf101.sem2.utils.records.*;

import java.awt.Color;


/**
 * An interface for drawable characters.
 * Contains only the information needed to draw the character.
 * 
 * @author Dag O.B. Himle
 *
 */
public interface IDrawableChar {
    

    /**
     * Getter for the position and direction of the character.
     * @return The position and direction of the character as an {@link PositionDirection} object.
     */
    default PositionDirection getPosDir(){
        return new PositionDirection(getPosition(), getDirection());
    }

    /**
     * Getter for the position of the character.
     * @return The position of the character as a {@link Position} object.
     */
    public Position getPosition();

    /**
     * Getter for the center of the character.
     * <p> The center is the position of the character plus half the size.
     * @return The center of the character as a {@link Position} object.
     */
    public Position getCenter();

    /**
     * Getter for the type of the character.
     * <p> The type is a character that is used to identify the character.
     * The types are:
     * <li> 'A' for an ant. </l>
     * <li> 'C' for a colony.
     * <li> 'F' for food.
     * <li> 'P' for pheromones.
     * <li> 'W' for walls.
     * @return
     */
    public char getDrawType();

    /**
     * Getter for the direction of the character.
     * <p> The direction is a double between 0 and 360.
     * <p> Returns 0 if direction is not immplemented for the character.
     * @return The direction of the character as a double between 0 and 360.
     */
    default double getDirection(){
        return 0;
    }


    /**
     * Getter for the radius of the character.
     * <p> Returns null if radius is not implemented for the character.
     * @return The radius of the character as an Integer.
     */
    default Integer getRadius(){
        return null;
    }

    /**
     * Getter for the color of the character.
     * <p> Returns null if color is not implemented for the character.
     * @return The color of the character as a {@link Color} object.
     */
    default Color getColor(){
        return null;
    }

}
