package no.uib.inf101.sem2.entities;

import no.uib.inf101.sem2.view.Inf101Graphics;
import no.uib.inf101.sem2.utils.RotatedImageIcon;
import no.uib.inf101.sem2.utils.records.Position;

import com.google.gson.annotations.Expose;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;


import java.awt.image.BufferedImage;
import java.awt.Color;
import java.util.List;
import java.util.HashMap;
import java.lang.reflect.Type;


/**
 * A colony of ants.
 * The colony has a position, a size, and a color.
 * The color is used to determine the sprite of the ants int the colony.
 * The size is the number of ants in the colony.
 * The position is the position of the colony.
 * <p> This class is intended to be used as a container for the ants.
 */
public class Colony implements IColony<Ant>, IDrawableChar{
    
    @Expose
    private final Position CENTER;
    @Expose
    private int size;
    @Expose
    private String color;
    
    private BufferedImage image;
    private List<Ant> ants = new java.util.ArrayList<>();
    private HashMap<Integer, RotatedImageIcon> rotatedSpritesMap = new HashMap<>();

    private final int ANT_SIZE = 60;
    private int antCount;



    /**
     * Constructor for a colony.
     * @param position The position of the colony.
     * @param size The size of the colony.
     * @param color The color of the colony.
     */
    public Colony(Position position, int size, String color) {
        this.CENTER = position;
        this.size = size;
        this.antCount = size;
        this.color = color;
        this.image = loadAntSprite(color);
        for (int i = 0; i < 361; i+= 1) {
            RotatedImageIcon rotatedSprite = new RotatedImageIcon(image, 0);
            rotatedSprite= rotatedSprite.rotateTo(i);
            rotatedSprite = rotatedSprite.scale(ANT_SIZE);
            getRotatedSpritesMap().put(i, rotatedSprite);
        }
        generateAnts();
    }   


    /**
     * Returns the sprite for an ant in the colony.
     * <p> The sprites are stored in a map, where the key is the rotation of the ant.
     * Sprite rotatations are handled by the colony to preserve memory.
     * And to avoid image distortion from too many operations on the same image.
     * @param rotation The rotation of the ant.
     */
    public RotatedImageIcon getRotatedSprite(int rotation) {
        rotation = (rotation % 360 + 360) % 360;
        //int nearestAngle = 5 * Math.round((float) rotation / 5);
        int nearestAngle = rotation;
        RotatedImageIcon sprite = rotatedSpritesMap.get(nearestAngle);
        if (sprite == null) {
            System.out.println("No sprite found for rotation: " + nearestAngle);
        }
        
        return sprite;
    }

    
    // IDrawableChar methods
    @Override
    public Position getPosition() {
        return new Position(CENTER.x() - getRadius(), CENTER.y() - getRadius());
    }
    
    @Override
    public Position getCenter() {
        return CENTER;

    }
    
    @Override
    public char getDrawType() {
        return 'C';
    }
    
    @Override
    public Integer getRadius() {
        if (size > 50){
            return 50/2;
        }else{
            return size/2;
        }
    }
    
    @Override
    public Color getColor() {
        switch (color) {
            case "red":
                return Color.RED;
            case "cyan":
                return Color.CYAN;
            case "blue":
                return Color.BLUE;
            case "yellow":
                return Color.YELLOW;
            default:
                return Color.PINK;
        }
    }

    /**
     * Returns the color of the colony as a string.
     * <p> This method is used to determine which colony an ant belongs to.
     * @return
     */
    public String getColorString() {
        return color;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Increased the size of the colony.
     * <p> This method also adds ants to the colony.
     * @param value The size to increase the colony with.
     */
    public void addSize(int value) {
        size += value;
    }

    @Override
    public java.util.Iterator<Ant> iterator() {
        return ants.iterator();
    }
    
    @Override
    public void addAnt(Ant ant) {
        ants.add(ant);
    }
    
    /**
     * Returns the ants in the colony.
     * @return A list of ants.
     */
    public List<Ant> getAnts() {
        return ants;
    }

    /**
     * Updates the number of ants in the colony to match the size.
     * <p> This method is used to add ants to the colony when the size is increased.
     */
    public void updateAntCount() {
        while (antCount < size) {
            Ant ant = new Ant(getCenter(), color);
            addAnt(ant);
            antCount++;
        }
    }

    private BufferedImage loadAntSprite(String color) {
        BufferedImage image = Inf101Graphics.loadImageFromResources("ant_"+color+".png");
        return image;
    }
    
    private HashMap<Integer, RotatedImageIcon> getRotatedSpritesMap() {
        if (rotatedSpritesMap == null) {
            rotatedSpritesMap = new HashMap<>();
        }
        return rotatedSpritesMap;
    }

    private void generateAnts() {
        for (int i = 0; i < size; i++) {
            Ant ant = new Ant(getCenter(), color);
            addAnt(ant);
        }
    }

    /**
     * Checks if the colony contains a position.
     * @param pos
     * @return true if the position is within the colony.
     */
    public boolean contains(Position pos){
        return getCenter().distance(pos) < getRadius();
    }


    /**
     * A deserializer for colonies.
     * This is used to deserialize colonies from json. Deseralizatio is necessary to load colonies from files.
     * As the it is bad practice to expose awt classes to gson.
     * <p> The deserializer is used by the Gson library.
     * 
     */
    public static class ColonyDeserializer implements JsonDeserializer<Colony> {
        @Override
        public Colony deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonObject = json.getAsJsonObject();
            Position position = context.deserialize(jsonObject.get("CENTER"), Position.class);
            int size = jsonObject.get("size").getAsInt();
            String color = jsonObject.get("color").getAsString();
            return new Colony(position, size, color);
        }
    }

}
