package no.uib.inf101.sem2.entities;

import no.uib.inf101.sem2.utils.records.Position;

import java.util.Random;

/**
 * An ant that can move around in the simulation.
 * <p> Implements {@link IAntData} and {@link IDrawableChar}
 */
public class Ant implements IAntData, IDrawableChar {

    // State
    private Position center;
    private String color;
    private double dir;
    private boolean carryingFood;

    // Counters
    private int counter = 1;
    private int time_searching = 0;
    
    //Constants
    private final int INTERVAL = 10;
    private final int ICON_SIZE = 9;


    public Ant(Position pos, String color) {
        this.center = pos;
        this.color = color;
        this.carryingFood = false;
        this.dir = new Random().nextDouble(0, 360);
    }

    @Override
    public void updatePosition(Position pos) {
        this.center = pos;
        this.counter ++;
        this.time_searching ++;
    }

    @Override 
    public boolean shouldDropPheromone() {
        if (counter % INTERVAL == 0) {
            this.counter = 0;
            return true;
        }
        return false;
    }


    @Override
    public Position getPosition() {
        return new Position(center.x() - getRadius(), center.y() - getRadius());
    }

    @Override
    public Position getCenter() {
        return center;
    }

    @Override
    public Integer getRadius() {
        return ICON_SIZE/2;
    }

    @Override
    public double getDirection() {
        return dir;
    }

    @Override
    public char getDrawType() {
        return 'A';
    }

    @Override
    public void setDirection(double dir) {
        this.dir = dir % 360;
    }
    
    @Override
    public boolean isCarryingFood() {
        return carryingFood;
    }

    /**
     * Gets the color of the ant as a string.
     * @return
     */
    public String getColonyColor() {
        return color;
    }
        
    // Behaviours for ants
    @Override
    public void pickUpFood() {
        this.carryingFood = true;
        this.time_searching = 0;
    }

    @Override
    public void dropFood() {
        this.carryingFood = false;
        this.time_searching = 0;
    }

    /**
     * Gets the time the ant has been searching for food or home.
     * @return
     */
    public int getTimeSearching() {
        return this.time_searching;
    }

    /**
     * Calculates a new position for the ant based on its current position and direction.
     * @param distance The distance the ant should move
     * @return The new position to check for collisions
    */
    public Position caluclateMove(double distance) {
        double correctedDirection = dir - 90; // Correct the direction to match the ants sprite
        double deltaX = Math.cos(Math.toRadians(correctedDirection)) * distance;
        double deltaY = Math.sin(Math.toRadians(correctedDirection)) * distance;
    
        int newX = (int) (center.x() + deltaX);
        int newY = (int) (center.y() + deltaY);
    
        return new Position(newX, newY);
    }

    /**
     * Checks if the ant contains the given position
     * @param pos The position to check
     * @return True if the position is within the ant, false otherwise
     */
    public boolean contains(Position pos) {
        return getCenter().distance(pos) < ICON_SIZE/2;
    }
}
