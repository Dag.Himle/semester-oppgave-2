package no.uib.inf101.sem2.entities;

import no.uib.inf101.sem2.utils.records.Position;

import java.awt.Color;

public class Pheromone implements IDrawableChar {
    public enum Type {
        TO_FOOD, TO_HOME
    }

    //Fields
    private Type type;
    private int strength;
    private final Position pos;
    private String color;
    
    //Constants
    private double DECAY_RATE = 2;
    private int SIZE = 2;

    public Pheromone(Type type, Position pos, String color) {
        this.type = type;
        this.color = color;
        this.pos = pos;
        this.strength = 1000;
    }

    /**
     * Returns the type of pheromone
     * as a {@link Type} enum
     * @return
     */
    public Type getType() {
        return type;
    }

    /**
     * Returns the strength of the pheromone
     * @return
     */
    public int getStrength() {
        return strength;
    }

    /**
     * Decays the pheromone by a constant amount
     * @return true if the pheromone has decayed to 0
     */
    public boolean decay() {
        strength-= DECAY_RATE;
        if (strength <= 100) {
            return true;
        }
        return false;
    }

    /**
     * Returns the size of the pheromone
     * as an Integer in pixels (diameter)
     * @return
     */
    public Integer getSize() {
        return SIZE;
    }
    
    @Override
    public Color getColor() {
        int opacity = (int) ((255 / 100.0) * strength/10);
        switch (type) {
            case TO_FOOD:
                return new Color(0, 255, 0, opacity);
            case TO_HOME:
                switch (color) {
                    case "red":
                        return new Color(255, 0, 0, opacity);
                    case "blue":
                        return new Color(0, 0, 255, opacity);
                    case "yellow":
                        return new Color(255, 255, 0, opacity);
                    case "cyan":
                        return new Color(0, 165, 255, opacity);
                }
                default: {
                    return new Color(0, 0, 0, opacity);
                }
            }
    }

    /**
     * Returns the color of the colony the ant that laid the pheromone belongs to
     * @return
     */
    public String getColonyColor() {
        return color;
    }

    // IDrawableChar methods
    @Override
    public Position getPosition() {
        return pos;
    }

    @Override
    public Position getCenter() {
        return pos;
    }

    @Override
    public Integer getRadius() {
        return SIZE / 2;
    }

    @Override
    public char getDrawType() {
        return 'P';
    }

    
}
