package no.uib.inf101.sem2.entities;

import no.uib.inf101.sem2.utils.records.Position;

import com.google.gson.annotations.Expose;

import java.awt.Color;

public class Food implements IDrawableChar {
    
    @Expose
    private final Position CENTER;
    @Expose
    private int foodAmount;

    public Food(Position position, int foodAmount) {
        this.CENTER = position;
        this.foodAmount = foodAmount;
    }

    @Override
    public Integer getRadius() {
        return (int) foodAmount / 2;
    }
    
    public int getFoodAmount() {
        return foodAmount;
    }
    
    public void removeFood(int amount) {
        foodAmount -= amount;
        if (foodAmount < 0) {
            foodAmount = 0;
        }
    }
    
    @Override
    public Position getPosition() {
        return new Position(CENTER.x() - getRadius(), CENTER.y() - getRadius());
    }

    @Override
    public Color getColor() {
        return Color.GREEN.darker();
    }


    @Override
    public Position getCenter() {
        return CENTER;
    }

    @Override
    public char getDrawType() {
        return 'F';
    }

    /**
     * Checks if the given position is within the food cache
     * @param pos
     * @return true if the position is within the food cache
     */
    public boolean contains(Position pos) {
        return getCenter().distance(pos) < getRadius();
    }
}

