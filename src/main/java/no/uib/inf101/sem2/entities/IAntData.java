package no.uib.inf101.sem2.entities;

import no.uib.inf101.sem2.utils.records.*;


/**
 * An interface for ants.
 * 
 * @author Dag O.B. Himle
 *
 */
public interface IAntData {



    /**
     * Update the position of the ant.
     * <p> Also updated the counter for the ant, which is used to determine when the ant should
     * drop pheromones.
     * 
     * @param pos The new position of the ant.
     */
    public void updatePosition(Position pos);

    /**
     * Returns a boolean indicating whether the ant should drop pheromones or not.
     * <p> The ant should drop pheromones every INTERVAL steps.
     * @return boolean true if the ant should drop pheromones, false otherwise.
     */
    public boolean shouldDropPheromone();

    /**
     * Update the direction of the ant.
     */
    public void setDirection(double dir);

    /**
     * Pick up food if the ant is on a food tile.
     */
    public void pickUpFood();

    /**
     * @return boolean true if the ant is carrying food, false otherwise.
     */
    public boolean isCarryingFood();

    /**
     * Drop whatever food the ant is carrying.
     */
    public void dropFood();


}
