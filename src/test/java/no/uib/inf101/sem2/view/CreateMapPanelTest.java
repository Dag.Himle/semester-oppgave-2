package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.model.AntSimulation;
import no.uib.inf101.sem2.model.SimMap;
import no.uib.inf101.sem2.view.components.MapPreviewComponent;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

public class CreateMapPanelTest {
    private AntSimulation simulation;
    private ScenarioPanel scenarioPanel;
    private CreateMapPanel createMapPanel;

    @BeforeEach
    public void setUp() {
        simulation = new AntSimulation();
        scenarioPanel = new ScenarioPanel(simulation, () -> {});
        createMapPanel = new CreateMapPanel(simulation, scenarioPanel, () -> {});
    }


}

