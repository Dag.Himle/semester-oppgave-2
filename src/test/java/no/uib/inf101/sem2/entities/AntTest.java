package no.uib.inf101.sem2.entities;

import no.uib.inf101.sem2.utils.records.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AntTest {

    private Ant ant;
    private Position initialPosition;
    private String color;

    @BeforeEach
    void setUp() {
        initialPosition = new Position(50, 50);
        color = "red";
        ant = new Ant(initialPosition, color);
    }

    @Test
    void updatePosition() {
        Position newPosition = new Position(60, 60);
        ant.updatePosition(newPosition);

        assertEquals(newPosition, ant.getCenter());
    }

    @Test
void shouldDropPheromone() {
    for (int i = 1; i < 10; i++) {
        if (i % 10 == 0) {
            assertTrue(ant.shouldDropPheromone());
        } else {
            assertFalse(ant.shouldDropPheromone());
        }
        ant.updatePosition(ant.getCenter());
    }

    assertTrue(ant.shouldDropPheromone());
}

    @Test
    void positionAndRadius() {
        assertEquals(initialPosition, ant.getCenter());
        assertEquals(4, ant.getRadius());
    }


    @Test
    void carryingFood() {
        assertFalse(ant.isCarryingFood());
        ant.pickUpFood();
        assertTrue(ant.isCarryingFood());
        ant.dropFood();
        assertFalse(ant.isCarryingFood());
    }

    @Test
    void calculateMove() {
        double distance = 10;
        Position newPosition = ant.caluclateMove(distance);
        double distanceMoved = initialPosition.distance(newPosition);

        assertEquals(distance, distanceMoved, 1);
    }

    @Test
    void contains() {
        Position positionInsideAnt = new Position(52, 52);
        Position positionOutsideAnt = new Position(40, 40);

        assertTrue(ant.contains(positionInsideAnt));
        assertFalse(ant.contains(positionOutsideAnt));
    }
}
