package no.uib.inf101.sem2.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.utils.records.Position;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

class WallTest {
    private Wall wall;

    @BeforeEach
    void setUp() {
        Position position = new Position(100, 100);
        int size = 10;
        wall = new Wall(position, size);
    }

    @Test
    void getPosition() {
        Position expected = new Position(95, 95);
        assertEquals(expected, wall.getPosition());
    }

    @Test
    void getCenter() {
        Position expected = new Position(100, 100);
        assertEquals(expected, wall.getCenter());
    }

    @Test
    void getColor() {
        assertEquals(Color.GRAY, wall.getColor());
    }

    @Test
    void getRadius() {
        assertEquals(5, wall.getRadius());
    }

    @Test
    void getDrawType() {
        assertEquals('W', wall.getDrawType());
    }

    @Test
    void contains() {
        Position inside = new Position(100, 100);
        Position outside = new Position(200, 200);

        assertTrue(wall.contains(inside));
        assertFalse(wall.contains(outside));
    }
}

