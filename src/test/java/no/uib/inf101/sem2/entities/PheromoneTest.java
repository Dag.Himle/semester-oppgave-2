package no.uib.inf101.sem2.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.utils.records.Position;

import static org.junit.jupiter.api.Assertions.*;

class PheromoneTest {
    private Pheromone pheromone;

    @BeforeEach
    void setUp() {
        Position position = new Position(50, 50);
        Pheromone.Type type = Pheromone.Type.TO_FOOD;
        String color = "red";
        pheromone = new Pheromone(type, position, color);
    }

    @Test
    void getType() {
        assertEquals(Pheromone.Type.TO_FOOD, pheromone.getType());
    }

    @Test
    void getStrength() {
        assertEquals(1000, pheromone.getStrength());
    }

    @Test
    void decay() {
        assertFalse(pheromone.decay());
        for (int i = 0; i < 450; i++) {
            pheromone.decay();
        }
        assertTrue(pheromone.decay());
    }

    @Test
    void getSize() {
        assertEquals(2, pheromone.getSize());
    }

    @Test
    void getPosition() {
        Position expected = new Position(50, 50);
        assertEquals(expected, pheromone.getPosition());
    }

    @Test
    void getCenter() {
        Position expected = new Position(50, 50);
        assertEquals(expected, pheromone.getCenter());
    }

    @Test
    void getRadius() {
        assertEquals(1, pheromone.getRadius());
    }

    @Test
    void getDrawType() {
        assertEquals('P', pheromone.getDrawType());
    }

    @Test
    void getColonyColor() {
        assertEquals("red", pheromone.getColonyColor());
    }
}
