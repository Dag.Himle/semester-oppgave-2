package no.uib.inf101.sem2.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.utils.records.Position;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

class ColonyTest {
    private Colony colony;

    @BeforeEach
    void setUp() {
        Position position = new Position(50, 50);
        int size = 10;
        String color = "red";
        colony = new Colony(position, size, color);
    }

    @Test
    void getPosition() {
        Position expected = new Position(45, 45); // 5 radius
        assertEquals(expected, colony.getPosition(), "Position is top left corner of colony and should be 5 radius away from center");
    }

    @Test
    void getCenter() {
        Position expected = new Position(50, 50);
        assertEquals(expected, colony.getCenter());
    }

    @Test
    void getDrawType() {
        assertEquals('C', colony.getDrawType());
    }

    @Test
    void getRadius() {
        int expectedRadius = 5;
        assertEquals(expectedRadius, colony.getRadius());
    }

    @Test
    void getColor() {
        assertEquals(Color.RED, colony.getColor());
        Colony blueColony = new Colony(new Position(50, 50), 10, "blue");
        assertEquals(Color.BLUE, blueColony.getColor());
        Colony yellowColony = new Colony(new Position(50, 50), 10, "yellow");
        assertEquals(Color.YELLOW, yellowColony.getColor());
        Colony cyanColony = new Colony(new Position(50, 50), 10, "cyan");
        assertEquals(Color.CYAN, cyanColony.getColor());
    }

    @Test
    void addSize() {
        int newSize = 20;
        colony.addSize(newSize);
        assertEquals(30, colony.getRadius()*2);
    }

    @Test
    void contains() {
        Position insidePosition = new Position(52, 52);
        Position outsidePosition = new Position(10, 10);
        assertTrue(colony.contains(insidePosition));
        assertFalse(colony.contains(outsidePosition));
    }
}

