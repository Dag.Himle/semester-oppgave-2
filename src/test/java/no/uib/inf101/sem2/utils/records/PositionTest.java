package no.uib.inf101.sem2.utils.records;

import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.Test;

public class PositionTest {

    @Test
    public void testDistance() {
        Position p1 = new Position(0, 0);
        Position p2 = new Position(3, 4);
        assertEquals(5, p1.distance(p2));
    }

    @Test
    public void testDirection() {
        Position p1 = new Position(0, 0);
        Position p2 = new Position(3, 4);
        assertEquals(143.1301, p1.directionTo(p2));
    }
    
}
