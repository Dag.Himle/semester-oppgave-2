package no.uib.inf101.sem2.utils;


import no.uib.inf101.sem2.model.SimMap;
import no.uib.inf101.sem2.utils.records.Position;
import no.uib.inf101.sem2.entities.Colony;
import no.uib.inf101.sem2.entities.Food;
import no.uib.inf101.sem2.entities.Wall;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SimMapIOTest {
    private SimMap testMap;
    private String testFileName;

    @BeforeEach
    void setUp() {
        testFileName = "testMap.json";

        // Create a test SimMap with some entities
        testMap = new SimMap(100, 100);
        testMap.setName("testMap");
        testMap.addColony(new Position(20, 20), 10, "Red");
        testMap.addFood(new Position(30, 30), 50);
        testMap.addWall(new Position(40, 40), 10);

        // Save the test SimMap to a file
        assertTrue(SimMapIO.saveMap(testMap, testFileName));
        System.out.println("Saved test map to " + testFileName);
    }

    @Test
    void testLoadMap() {
        // Load the test SimMap from the file
        SimMap loadedMap = SimMapIO.loadMap(testFileName);

        assertNotNull(loadedMap);
        assertEquals(testMap.getWidth(), loadedMap.getWidth());
        assertEquals(testMap.getHeight(), loadedMap.getHeight());

        List<Colony> loadedColonies = loadedMap.getColonies();
        List<Colony> originalColonies = testMap.getColonies();
        assertEquals(originalColonies.size(), loadedColonies.size());

        List<Food> loadedFood = loadedMap.getFood();
        List<Food> originalFood = testMap.getFood();
        assertEquals(originalFood.size(), loadedFood.size());

        List<Wall> loadedWalls = loadedMap.getWalls();
        List<Wall> originalWalls = testMap.getWalls();
        assertEquals(originalWalls.size(), loadedWalls.size());
    }

    @AfterEach
    void tearDown() {
        //Delete the test file
        File testFile = new File("src/main/json/maps", testFileName);
        assertTrue(testFile.delete());
    }
}
