package no.uib.inf101.sem2.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Image;
import java.awt.Color;
import java.awt.Graphics2D;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.awt.image.BufferedImage;

public class RotatedImageIconTest {

    private RotatedImageIcon icon;

    @BeforeEach
    public void setUp() {
        int width = 100;
        int height = 100;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = image.createGraphics();
        g2d.setColor(Color.RED);
        g2d.fillRect(0, 0, width, height);
        g2d.dispose();
        icon = new RotatedImageIcon(image);
    }

    @Test
    public void testInitialAngle() {
        assertEquals(180, icon.getInitialAngle());
    }

    @Test
    public void testRotate() {
        RotatedImageIcon rotatedIcon = icon.rotate(45);
        assertEquals(225, rotatedIcon.getInitialAngle());
    }

    @Test
    public void testRotatePixelComparison() {
        double angle = 45;
        RotatedImageIcon rotatedIcon = icon.rotate(angle);
        Image rotatedImage = rotatedIcon.getImage();

        BufferedImage referenceImage = new BufferedImage(rotatedImage.getWidth(null), rotatedImage.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = referenceImage.createGraphics();
        g2d.setColor(Color.RED);
        g2d.rotate(Math.toRadians(angle), referenceImage.getWidth() / 2, referenceImage.getHeight() / 2);
        g2d.fillRect(0, 0, referenceImage.getWidth(), referenceImage.getHeight());
        g2d.dispose();

        assertEquals(referenceImage.getWidth(), rotatedImage.getWidth(null));
        assertEquals(referenceImage.getHeight(), rotatedImage.getHeight(null));
        assertEquals(referenceImage.getRGB(0, 0), ((BufferedImage)rotatedImage).getRGB(0, 0));
        assertEquals(referenceImage.getRGB(referenceImage.getWidth() - 1, referenceImage.getHeight() - 1), ((BufferedImage)rotatedImage).getRGB(rotatedImage.getWidth(null) - 1, rotatedImage.getHeight(null) - 1));
    }

    @Test
    public void testRotate2() {
        RotatedImageIcon rotatedIcon = icon.rotate(181);
        assertEquals(1, rotatedIcon.getInitialAngle());
    }

    @Test
    public void testRotateTo() {
        RotatedImageIcon rotatedIcon = icon.rotateTo(270);
        assertEquals(270, rotatedIcon.getInitialAngle());
    }

    @Test
    public void testScale() {
        RotatedImageIcon scaledIcon = icon.scale(50);
        assertEquals(50, scaledIcon.getIconWidth());
        assertEquals(50, scaledIcon.getIconHeight());
    }
}