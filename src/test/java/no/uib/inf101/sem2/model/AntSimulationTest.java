package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.entities.Colony;
import no.uib.inf101.sem2.entities.IDrawableChar;
import no.uib.inf101.sem2.utils.records.Position;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AntSimulationTest {
    private AntSimulation antSimulation;

    @BeforeEach
    void setUp() {
        antSimulation = new AntSimulation();
    }

    @Test
    void testGetColonies() {
        List<Colony> colonies = antSimulation.getColonies();
        assertNotNull(colonies);
    }

    @Test
    void testGetDrawableEntities() {
        List<IDrawableChar> drawableEntities = antSimulation.getDrawableEntities();
        assertNotNull(drawableEntities);
    }

    @Test
    void testLoadMap() {
        String mapName = "Test_Map.json";
        antSimulation.loadMap(mapName);
        List<Colony> colonies = antSimulation.getColonies();
        assertFalse(colonies.isEmpty());
    }

    @Test
    void testUpdateAnt(){
        SimMap map = new SimMap(100, 100);
        Colony colony = new Colony(new Position(20, 20), 10, "Red");
        map.addColony(colony.getPosition(), colony.getAnts().size(), colony.getColorString());
        antSimulation = new AntSimulation(map);
        antSimulation.step(false);
        assertEquals(10, colony.getAnts().size());
        for (Colony c : antSimulation.getColonies()){
            assertFalse(Arrays.equals(c.getAnts().toArray(), colony.getAnts().toArray()));
        }


    }
}
