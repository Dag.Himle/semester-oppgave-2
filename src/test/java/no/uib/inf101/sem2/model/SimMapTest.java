package no.uib.inf101.sem2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.entities.Colony;
import no.uib.inf101.sem2.entities.Food;
import no.uib.inf101.sem2.entities.Wall;
import no.uib.inf101.sem2.utils.records.Position;


import static org.junit.jupiter.api.Assertions.*;

class SimMapTest {
    private SimMap simMap;

    @BeforeEach
    void setUp() {
        simMap = new SimMap(400, 400);
    }

    @Test
    void testAddAndRemoveColony() {
        Position position = new Position(100, 100);
        simMap.addColony(position, 50, "red");
        assertEquals(1, simMap.getColonies().size());

        Colony colony = simMap.getColonies().get(0);
        simMap.removeColony(colony);
        assertEquals(0, simMap.getColonies().size());
    }

    @Test
    void testAddAndRemoveFood() {
        Position position = new Position(200, 200);
        simMap.addFood(position, 100);
        assertEquals(1, simMap.getFood().size());

        Food food = simMap.getFood().get(0);
        simMap.removeFood(food);
        assertEquals(0, simMap.getFood().size());
    }

    @Test
    void testAddAndRemoveWall() {
        Position position = new Position(300, 300);
        simMap.addWall(position, 10);
        assertEquals(1, simMap.getWalls().size());

        Wall wall = simMap.getWalls().get(0);
        simMap.removeWall(wall);
        assertEquals(0, simMap.getWalls().size());
    }

    @Test
    void testPositionInBounds() {
        Position inBounds = new Position(50, 50);
        Position outOfBounds = new Position(450, 450);

        assertTrue(simMap.isPositionInBounds(inBounds));
        assertFalse(simMap.isPositionInBounds(outOfBounds));
    }

    @Test
    void testDeleteObjectAt() {
        Position colonyPosition = new Position(100, 100);
        Position foodPosition = new Position(200, 200);
        Position wallPosition = new Position(300, 300);

        simMap.addColony(colonyPosition, 50, "red");
        simMap.addFood(foodPosition, 100);
        simMap.addWall(wallPosition, 10);

        simMap.deleteObjectAt(colonyPosition);
        simMap.deleteObjectAt(foodPosition);
        simMap.deleteObjectAt(wallPosition);

        assertEquals(0, simMap.getColonies().size());
        assertEquals(0, simMap.getFood().size());
        assertEquals(0, simMap.getWalls().size());
    }
}
