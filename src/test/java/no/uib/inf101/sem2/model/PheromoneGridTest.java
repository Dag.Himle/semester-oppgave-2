package no.uib.inf101.sem2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.entities.Pheromone;
import no.uib.inf101.sem2.utils.records.Position;

import static org.junit.jupiter.api.Assertions.*;

class PheromoneGridTest {
    private PheromoneGrid pheromoneGrid;
    private int cellSize;
    private int gridWidth;
    private int gridHeight;

    @BeforeEach
    void setUp() {
        cellSize = 10;
        gridWidth = 10;
        gridHeight = 10;
        pheromoneGrid = new PheromoneGrid(cellSize, gridWidth, gridHeight);
    }

    @Test
    void addPheromone() {
        Pheromone pheromone = new Pheromone(Pheromone.Type.TO_FOOD, new Position(5, 5), "red");
        pheromoneGrid.addPheromone(pheromone);
        assertEquals(1, pheromoneGrid.getPheromonesInRadius(new Position(5, 5), cellSize).size());
    }

    @Test
    void getPheromonesInRadius() {
        Pheromone pheromone1 = new Pheromone(Pheromone.Type.TO_FOOD, new Position(5, 5), "red");
        Pheromone pheromone2 = new Pheromone(Pheromone.Type.TO_HOME, new Position(15, 15), "blue");
        pheromoneGrid.addPheromone(pheromone1);
        pheromoneGrid.addPheromone(pheromone2);

        assertEquals(1, pheromoneGrid.getPheromonesInRadius(new Position(5, 5), cellSize).size());
        assertEquals(2, pheromoneGrid.getPheromonesInRadius(new Position(10, 10), cellSize * 2).size());
    }

    @Test
    void updatePheromones() {
        Pheromone pheromone = new Pheromone(Pheromone.Type.TO_FOOD, new Position(5, 5), "red");
        pheromoneGrid.addPheromone(pheromone);

        for (int i = 0; i < 450; i++) {
            pheromoneGrid.updatePheromones();
        }

        assertEquals(0, pheromoneGrid.getPheromonesInRadius(new Position(5, 5), cellSize).size());
    }

    @Test
    void getDrawablePheromones() {
        Pheromone pheromone1 = new Pheromone(Pheromone.Type.TO_FOOD, new Position(5, 5), "red");
        Pheromone pheromone2 = new Pheromone(Pheromone.Type.TO_HOME, new Position(15, 15), "blue");
        pheromoneGrid.addPheromone(pheromone1);
        pheromoneGrid.addPheromone(pheromone2);

        assertEquals(2, pheromoneGrid.getDrawablePheromones().size());
    }
}

